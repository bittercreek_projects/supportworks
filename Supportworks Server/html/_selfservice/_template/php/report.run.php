<?php 	//-- NWJ - load xml file based on input $name
	//-- include common functions
	include("ITSMF/xmlmc/common.php");
	include("ITSMF/xmlmc/classReport.php");
	$xmlReportFile = gv("reportname");

	//-- create new report class
	$boolError=false;
	$oReport = new classReport;
	if($oReport->load_report($xmlReportFile))
	{
		//-- report xml loaded ok
		$strReportInputHTML = $oReport->output_input();
	}
	else
	{
		$boolError=true;
		$strReportInputHTML = "ERROR:Failed to load report (".htmlentities($xmlReportFile,ENT_QUOTES,'UTF-8')."). Please contact your supportworks administrator.";
	}

	//-- NWJ - 02.01.2008 - added parse_context_vars to report title.

?>


<div class="boxWrapper" style="margin:0px auto 0px auto;width:95%">
<img src="img/structure/box_header_left.gif" width="6" height="11" alt="" border="0"/><div class="boxMiddle">
<div class="boxContent" >

<h2><?php echo parse_context_vars($oReport->get_title());?> - Criteria</h2>
<form id="reportform" action="php/report.submit.php">
	
	<?php echo  $strReportInputHTML;?>

	<input type="hidden" id="reportname" value="<?php echo htmlentities($xmlReportFile,ENT_QUOTES,'UTF-8');?>">
</form>

<?php if(!$boolError)
{
?>
	<table width="100%">
		<tr>
			<td align="center">
				<input type="button" id="btn_submit" onclick="app.submit_form('reportform');" value="Submit <?php echo parse_context_vars($oReport->get_title());?> Report" class="buttonNext" style="font-size:125%;" />
			</td>
		</tr>
	</table>
<?php }

$oReport = null;
?>

<!-- end of box content -->
<div class="spacer">&nbsp;</div>
</div>
</div>
<div class="boxFooter"><img src="img/structure/box_footer_left.gif"/></div>
</div>

<?php 	//-- NWJ - load xml file based on input $name
	//-- include common functions
	include("ITSMF/xmlmc/common.php");
	include("ITSMF/xmlmc/classReport.php");

	$arrReportCriteria = Array();

	//-- get passed in form data
	//-- loop through posted vars and id each question
	foreach ($HTTP_GET_VARS as $key => $val)
	{
		//-- Escape any $_Request keys and value
		$key = htmlentities($key);
		$val = htmlentities($val);
		
		//-- check if we need to split data
		if(strpos($val,ANSWER_SPLIT)!==false)$val = str_replace(ANSWER_SPLIT," and ",$val);
		
		if(strpos($key,"_")!==false)
		{
			$arrInfo = explode("_",$key,2);
			$tblName = $arrInfo[0];
			$colName = $arrInfo[1];
			$bindkey = $tblName .".".$colName;
			$arrReportCriteria[$bindkey] = stripslashes($val);
		}
		else
		{
			//-- something else
		}
	}
	
	
	$xmlReportFile = gv("reportname");

	//-- create new report class
	$boolError=false;
	$oReport = new classReport;
	if($oReport->load_report($xmlReportFile))
	{
		//-- report xml loaded ok - construct select statement and out put results
		$strReportOutputHTML = $oReport->perform_search($arrReportCriteria);
	}
	else
	{
		$boolError=true;
		$strReportOutputHTML = "ERROR:Failed to load report (".htmlentities($xmlReportFile,ENT_QUOTES,'UTF-8')."). Please contact your supportworks administrator.";
	}

?>


<div class="boxWrapper" style="margin:0px auto 0px auto;width:95%">
<img src="img/structure/box_header_left.gif" width="6" height="11" alt="" border="0"/><div class="boxMiddle">
<div class="boxContent" >

<!-- box content -->
<h2 style="width:100%;"><?php echo $oReport->get_title();?> - Results</h2>

<?php echo  $strReportOutputHTML;?>
<!-- end of box content -->
<div class="spacer">&nbsp;</div>
</div>
</div>
<div class="boxFooter"><img src="img/structure/box_footer_left.gif"/></div>
</div>

<?php 	$oReport = null;
?>
<?php

//--
//-- add any additional access functions here (call them in the "accessfunction" attribute in menuset.xml)


//-- return if logged in customer is a manager
function customer_is_manager()
{
	return $_SESSION['userdb_flg_manager'];
}

//-- generate the request types menu for nav bar
//-- get default request types and get customer related request types
function generate_request_types_items()
{
	$strItemHTML = "";
	$itemsRS = get_default_customerrequest_types($_SESSION['customerid']);
	while(!$itemsRS->eof)
	{
		$strChildItem = $itemsRS->f('ssmenu');
		if($strChildItem=="")$strChildItem="Unkown Request";
		$strPHPcontent = "content/servicerequest.launch.php?in_requestid=".$itemsRS->f('pk_code');
		$strItemHTML .= "<li><a href='#' onclick='menu_item_selected(this);' aparent='0' expanded='0' phpactions='' phpcontent='".$strPHPcontent."' >".$strChildItem."</a>";
		$itemsRS->movenext();
	}
	return $strItemHTML;
}


?>

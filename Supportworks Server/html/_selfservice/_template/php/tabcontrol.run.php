<?php 	include("ITSMF/xmlmc/common.php");

	//-- create a tab control class
	$oTabControl = load_tab_form(gv('tabXML'));
?>

<div class="boxWrapper" style="margin:0px auto 0px auto;width:95%;">
<img src="img/structure/box_header_left.gif" width="6" height="11" alt="" border="0"/><div class="boxMiddle">
<div class="boxContent">
<div class="spacer">&nbsp;</div>
	<!-- box content -->
	<h2><?php echo $oTabControl->get_title();?></h2>
	
	<p><?php echo nl2br($oTabControl->get_desc());?></p>

	<?php echo $oTabControl->draw('','');?>

	<!-- end of box content -->
	<div class="spacer">&nbsp;</div>
</div>
</div>
<div class="boxFooter"><img src="img/structure/box_footer_left.gif"/></div>
</div>


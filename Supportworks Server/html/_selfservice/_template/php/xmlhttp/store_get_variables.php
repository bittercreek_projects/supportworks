<?php

//-- NWJ - work around for GET limit for IE when sending data in SSPI


//-- start php session
session_start();

$in_httprequestid = $_REQUEST['httpreqid'];

//-- not set yet
if(!isset($_SESSION[$in_httprequestid]))$_SESSION[$in_httprequestid] = Array();

//-- now loop request data and store 
foreach ($_REQUEST as $key => $val)
{
	if($key=='httpreqid')continue;
	if($key=='PHPSESSID')continue;
	$_SESSION[$in_httprequestid][$key] .= $val;
}

?>
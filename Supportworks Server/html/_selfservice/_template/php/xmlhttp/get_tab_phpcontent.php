<?php
	//-- NWJ - 04.04.2007 - given a tab id and a tab control name get the type of action and process accordingly
	//-- returns html to place inside tab div area

	include_once("ITSMF/xmlmc/common.php");
	if(gv('tabid')=="" || gv('tabcontrolname')=="")
	{
		//-- we do not have the information we need
		$strHTML = "<center>The tab content could not be loaded. Please contact your Supportworks administrator</center>";
		echo $strHTML;
		exit;
	}	

	//-- load xml file and get the tab node (we can then check its type and process as required)
	$oTabControl = load_tab_form(gv('tabcontrolname'));
	$xmlNode = $oTabControl->get_tabnode(gv('tabid'));
	if($xmlNode==false)
	{
		$strHTML = "<center>The tab (".gv('tabid').") content could not be loaded. Please contact your Supportworks administrator</center>";
		echo $strHTML;
		exit;
	}
	else
	{
		//-- get content type and process as needed
		$strType = $xmlNode->get_attribute("contenttype");
		if($strType=="datatable")
		{
			//-- load datatable
		}
	}
?>
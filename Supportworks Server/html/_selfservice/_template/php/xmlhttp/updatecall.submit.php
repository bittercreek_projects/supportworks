<?php

//-- NWJ - Example that will loop through passed in vars and update a call.

//-- must have a var called opencall_callref so in your form have a <input type='hidden' name='opencall.callref' value='123'>

//-- then will process any fields that have opencall.columnname and exttbl_tablename_.columnname where exttable could be "incdetail"
//-- example fields...

//-- <input type='input' name='opencall.impact_level' value='High'>
//-- <input type='input' name='extbl_incdetail_.description' value='This is my description that goes into extended table'>


	include("ITSMF/xmlmc/common.php");
	//-- This script will loop through posted vars find all those with
	//-- opencall_ and updatedb_

	//-- if required opencall fields (like priority) are not passed in will use session default as defined in self serv config
	$arrOpencallValues = Array();
	$arrUpdatedbValues = Array();
	$arrExtendedTables = Array();

	//-- loop through posted vars and id each question
	foreach ($_REQUEST as $key => $val)
	{
		//-- check if we need to split data
		if(strpos($val,ANSWER_SPLIT)!==false)$val = str_replace(ANSWER_SPLIT," and ",$val);

		//echo $key . " : " . $val . "<br./>";
		if(strpos($key,"opencall_")!==false)
		{
			if($key=="opencall_callref") continue;

			$arrInfo = explode("_",$key,2);
			$colName = $arrInfo[1];
			$arrOpencallValues[$colName] = $val;
		}
		else if (strpos($key,"updatedb_")!==false)
		{
			//-- check opencall priority option
			$arrInfo = explode("_",$key,2);
			$colName = $arrInfo[1];
			$arrUpdatedbValues[$colName] = $val;
		}
		else if (strpos($key,"extbl_")===0)
		{
			//-- we want to store info into an extended table (in hnlt element has id="extbl_tablname_.colname"
			$arrInfo = explode("extbl_",$key,2);
			$strTableAndColName = $arrInfo[1];

			$arrTblInfo = explode("__",$strTableAndColName,2);

			$strExtTable = $arrTblInfo[0];
			$strExtCol = $arrTblInfo[1];

			//-- echo $strExtTable . " : " . $strExtCol;
			if(!isset($arrExtendedTables[$strExtTable]))$arrExtendedTables[$strExtTable] = Array();
			$arrExtendedTables[$strExtTable][$strExtCol] = $val;
		}
		else
		{
			//-- something else
		}
	}

		
	//-- create helpdesk session
	$hdConn = new CSwHDsession($_SESSION['connector_instance']);
	if(!$hdConn->open_hd_connection())
	{
		echo $hdConn->LastError;
		exit;
	}

	//-- if we have opencall values then proces opencall update values - note not the same as a call diary update
	if(!empty($arrOpencallValues))
	{
		//--
		//-- check if we have opencall fields to update
		if($hdConn->StartCallValuesUpdate($opencall_callref))
		{
			foreach ($arrOpencallValues as $strTargetField => $varFieldValue)
			{
				//--
				//-- exclude any special fields like status, priority, callref - could add some code to handle updates to call status and priority
				$boolProcess=true;
				switch($strTargetField)
				{
					case "status":
					case "priority":
					case "callref":
						$boolProcess=false;
						break;;
				}
		
				if($boolProcess)
				{
					//-- uncomment to see fields you updated - note blank fields are processed
					//-- echo $strTargetField . " : " . $varFieldValue ."</br>";

					//-- send the value
					$intType = swdti_getdatatype("opencall.".$strTargetField);
					$boolNum = ($intType==8||$intType==-1)?false:true;
					$hdConn->SendCallValue($strTargetField,$varFieldValue,$boolNum);
				}
			}

			if(!$hdConn->CommitCallAction())
			{
				echo $hdConn->LastError;
				exit;
			}

		}	//--
	}

	//--
	//-- update any extended table values
	$swDATA = database_connect("swdata");
	foreach ($arrExtendedTables as $table_name => $arr_columns)
	{
		$strSetFields = "";
		//-- create insert cols
		foreach ($arr_columns as $column_name => $column_value)
		{
			if($strSetFields != "")$strSetFields .= ",";

			$intType = swdti_getdatatype($table_name.".".$column_name);
			$strEncaps = ($intType==8||$intType==-1)?"'":"";

			$strSetFields .= $column_name = $strEncaps . PrepareForSql($column_value) . $strEncaps;
		}

		//-- apply update
		if($strSetFields!="")
		{
			//-- expects ext table to have a primary key of callref 
			$strExtendedUpdate = "update "	. strtolower($table_name) . " set " . $strSetFields . " where callref = " . PrepareForSql(opencall_callref);
			if($swDATA->Query($strExtendedUpdate)==false)
			{
				//-- failed - old version sometime shas ext tables with key of opencall
				$strExtendedUpdate = "update "	. strtolower($table_name) . " set " . $strSetFields . " where opencall = " . PrepareForSql($opencall_callref);
				if($swDATA->Query($strExtendedUpdate)==false)
				{
					echo "Failed to process extended table update";
					exit;
				}
			}
		}
	}


?>
<?php
	//-- NWJ - 04.04.2007 - given a tab id and a tab control name get the type of action and process accordingly
	//-- returns html to place inside tab div area

	include_once("ITSMF/xmlmc/common.php");

	//-- returns html to place inside tab div area
	if(gv('tabid')=="" || gv('tabcontrolname')=="")
	{
		//-- we do not have the information we need
		$strHTML = "<center>The tab content could not be loaded. Please contact your Supportworks administrator</center>";
		echo $strHTML;
		exit;
	}

	$oTabControl = load_tab_form(gv('tabcontrolname'));
	$xmlNode = $oTabControl->get_tabnode(gv('tabid'));
	if($xmlNode==false)
	{
		$strHTML = "<center>The tab (".htmlentities(gv('tabid'),ENT_QUOTES,'UTF-8').") content could not be loaded. Please contact your Supportworks administrator</center>";
		echo $strHTML;
		exit;
	}
	else
	{
		//-- get content type and process as needed
		$strType = $xmlNode->get_attribute("contenttype");
		if(($strType=="datatable") || (gv("override")=="1"))
		{
			//-- load datatable
			$xmlDataTable = $xmlNode->get_elements_by_tagname("datatable");
			$aDataTable = load_data_table($xmlDataTable[0]);
			if($aDataTable!=false)
			{
				echo $aDataTable->output_data_table(gv('orderby'),gv('sortdir'),gv('filterbyoption'),gv('startatrow'),gv('showrowcount'),gv('lookfor'),gv('lookin'));
			}
		}
		else
		{
			$strHTML = "<center>The tab (".htmlentities($tabid,ENT_QUOTES,'UTF-8').") content is not a data table. Please contact your Supportworks administrator</center>";
			echo $strHTML;
			exit;
		}
	}
?>
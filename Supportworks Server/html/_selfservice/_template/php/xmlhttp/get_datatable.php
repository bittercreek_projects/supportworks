<?php
	//-- NWJ - 04.04.2007 -given a datatable name get structure and process

	include_once("ITSMF/xmlmc/common.php");

	//-- returns html to place inside tab div area
	if(gv('tablecontrol')=="")
	{
		//-- we do not have the information we need
		$strHTML = "<center>The table definition name was not provided and could not be loaded. Please contact your Supportworks administrator</center>";
		echo $strHTML;
		exit;
	}

	$xmlTable = load_table_xml(gv('tablecontrol'));
	$xmlDataTable = $xmlTable->get_elements_by_tagname("datatable");
	$aDataTable = load_data_table($xmlDataTable[0]);
	if($aDataTable!=false)
	{
		echo $aDataTable->output_data_table(gv('orderby'),gv('sortdir'),gv('filterbyoption'),gv('startatrow'),gv('showrowcount'),gv('lookfor'),gv('lookin'));
	}


?>
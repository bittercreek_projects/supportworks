<?php 	include("ITSMF/xmlmc/common.php");

	$strMessage="";
	$uploading= gv('in_uploading');

	//-- get file info
	$tmp_name = $_FILES['afile']['tmp_name'];
	$file_name = $_FILES['afile']['name'];
	$file_type = $_FILES['afile']['type'];

	$prefix = 'wssmupl_';
	$strMessage = "";

	if($uploading=="1")
	{
		//-- check if key matches
		if(!check_secure_key($prefix.'key'))
		{
			//-- set uploading to zero (determines if action is being taken)
			$strMessage = "<font color='red'>Authentication Failure. Failed to upload file. <br>Please contact your Supportworks administrator.</font>";
			$uploading = 0;
		}	
	}

	$strKey = generate_secure_key($prefix);
	$_SESSION[$prefix.'key'] = $strKey;

	//-- check if uploading
	$update_callref = gv('in_callref');
	$update_desc = gv('in_description');

	if(($update_callref!="")&&($uploading=="1"))
	{

		if(($tmp_name!="")&&($file_name!=""))
		{
			if($update_desc=="")$update_desc = "The customer ".$_SESSION['customername']." has attached the file (".$file_name.") to this request.";

			//-- create helpdesk session
			$hdConn = new CWSSMActions($_SESSION['connector_instance']);
			if(!$hdConn->StartCallUpdate($update_callref, $update_desc,5,"Customer Action","Attached File","1"))
			{
				$strMessage = $hdConn->LastError;
			}
			else
			{
				//- -sne dfile to sw hd
				$hdConn->sendfile($tmp_name,$file_name,$file_type);
				$res= $hdConn->CommitCallAction("customerUpdateCall");
				if($res)
				{
					$strMessage = "<font color='green'>The file was successfully attached to the following call ".swcallref_str($update_callref) ."</font>";
				}
				else
				{
					$strMessage = "<font color='red'>The file could not be attached to the following call ".swcallref_str($update_callref)."</font>";
				}

			}
		}
		else
		{
			$strMessage = "<font color='red'>The file could not be attached to the call ".swcallref_str($update_callref)." as it failed to upload.</font>";
		}

	}
	else if($update_callref=="")
	{
		$strMessage = "<font color='red'>The file cannot be attached to the call as a call reference was not provided.</font>";
	}
?>
<html>
<link href="../../css/structure_ss.css" rel="stylesheet" type="text/css" />
<link href="../../css/elements.css" rel="stylesheet" type="text/css" />
<script>

	function submit_file()
	{
		var uploadfileForm = document.forms['fileloader'];
		var oDiv = document.getElementById('file-uploader');
		if(oDiv==null)return true;
		if(oDiv.style['display']=='none')
		{
			//-- position the div
			document.getElementById('upload-msg').style.display="none";
			oDiv.style['display'] = "block";
			setTimeout("process_file_upload()",1000);
		}
		return true;
	}

	function process_file_upload()
	{
		if(document.forms['fileloader']!=null)
		{
			document.forms['fileloader'].submit();		
		}
	}

	//--
	//-- hide file uploading message
	function hide_uploadingmsg()
	{

		var oDiv = document.getElementById('file-uploader');
		if(oDiv==null)return true;
		oDiv.style['display']='none';
		document.getElementById('upload-msg').style.display="inline";		
	}

	//-- eof file uplaod processing

</script>
<body style="background-color:#ffffff;overflow:hidden;" onload="hide_uploadingmsg();">

		<div id="file-uploader"><table height="100%" width="100%"><tr><td align="center">Uploading file...please wait..</td></tr></table></div>
		
		<form name="fileloader" action="uploadfile.php" target="_self" enctype="multipart/form-data" method="post" style="width:600px">
			<input type="file" name="afile" style="width:550px;" size="92"/>
			<input type="hidden" name="in_callref" value="<?php echo $update_callref;?>"/><br/>
			<input type="hidden" name="in_uploading" value="1"/>
			<input type="hidden" id="<?php echo $prefix;?>key" name="<?php echo $prefix;?>key" value="<?php echo $strKey;?>"/>
		</form>
		<br/>
		<form style="width:600px">
			<center>
				<input type="button" value="Click here to submit the file" onClick="submit_file();">
			</center>
			<br/>
			<p id='upload-msg'><center><?php echo $strMessage;?></center></p>
		</form>
</body>
</html>

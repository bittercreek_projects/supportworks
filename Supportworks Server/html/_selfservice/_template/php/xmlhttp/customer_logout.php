<?php
	include_once("ITSMF/xmlmc/common.php");
	session_start();
	$host = $_SESSION['server_name'];
	$xmlmc = new XmlMethodCall();
	$xmlmc->Invoke("session","selfServiceLogoff",$host);
	$strLastError = $xmlmc->GetLastError();
	if($strLastError=="")
	{
		echo "OK";
	}
	else
	{
		if($strLastError=='[0005] Invalid session. Please establish a session first')
			echo "OK";
		else
			echo $strLastError;	
	}
	return;

?>
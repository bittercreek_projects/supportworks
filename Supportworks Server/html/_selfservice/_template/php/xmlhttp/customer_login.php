<?php
	//-- NWJ - 02.04.2007 - Login script. To be called from browser using http request.
	//-- includes
	if($_SESSION['customerid']!="")
	{
		session_destroy();
		session_start();
	}


	include_once("../../_ssconfig.php");
	include_once("ITSMF/xmlmc/classCustomerSession.php");
	include_once("SwNoCachePage.php");
	include_once("stdinclude.php");


	//-- Create an instance of our session manager
	$customersession = new classCustomerSession;
	$customersession->initialiseCustomerSession(_INSTANCE_NAME, _SERVER_NAME, _INSTANCE_PATH);


	//-- nwj - check for any encoded data
	//-- get encoded data then split by & to get vars - see portal.control.js openWin function
	$in_data = base64_decode(gv('ied'));
	$arr_data = explode("&", $in_data);
	foreach ($arr_data as $pos => $aVariable) 
	{
		$arr_var = explode("=", $aVariable);
		$_GET[$arr_var[0]]=$arr_var[1];
		$GLOBALS[$arr_var[0]]=$arr_var[1];

	}
	//-- end of decoding
	//--


	$loginid   = gv('loginid');
	$loginpass =  gv('loginpass');
	if($loginid=="")
	{
		if(isset($_SERVER['REMOTE_USER']))
		{
			$cred = explode("\\",$_SERVER['REMOTE_USER']);
			if (count($cred) == 1) array_unshift($cred, "(no domain info - perhaps SSPIOmitDomain is On)");
			list($domain, $user) = $cred;

			$loginid   =  $user;
			$loginpass = '_SSPI_'.$domain;
		}
	}


	//-- validate customer login
	$strResult= "OK";
	$strLoginResult = $customersession->CustomerLogin($loginid, $loginpass);
	switch($strLoginResult)
	{
		case LOGIN_FAILED_NO_SERVER:
			$strResult="Unable to connect to the Supportworks Server. Please contact your system administrator";
			break;
		case LOGIN_FAILED_NO_NOACCESS:
			$strResult="you do not have permission to use the service portal. Please contact the support helpdesk.";
			break;
		case LOGIN_FAILED_CHECK_CREDENTIALS:
		case LOGIN_FAILED_NOT_NOTREGISTERED:
			$strResult="Authorisation failure, please check your customer ID and / or password and try again";
			break;
		case LOGIN_FAILED_SERVER_ERROR:
			$strResult="Server or Web configuration error. Contact your system administrator";
			break;
		case CUSTOMER_LOGIN_OK:

			//--
			//-- store the install path
			$_SESSION['INSTANCE_INSTALL'] = "_selfservice\\". _INSTANCE_NAME . "\\";
			$strResult.= "OK";

			//-- set cookie to remember me
			$remember   = gv('remember');
			if($remember==1)
			{
				//-- store 30 day cookie using userid as array pointer (means cookie can store mulitple user info)
				//-- F0094802
				$strAppend = "";
				if($_SERVER["HTTPS"]=="on")
					$strAppend = "; Secure";
				header( "Set-Cookie: swssusers[".$loginid."]=".$_SESSION['userdb_firstname'] . ' ' .  $_SESSION['userdb_surname']."; Expires=".date('D, d-M-Y H:i:s GMT',time()+60*60*24*30)."; httpOnly; path=/".$strAppend );
			}


			//-- nwj - 05.08.2008 - set customer pk value (for use when getting call list and saving calls) i.e. this is the value to stick into opencall.cust_id
			//-- note this will not always be the control id as setup in the selfservice config tool
			if (defined('_CUST_OC_JOIN_COLUMN')&&(_CUST_OC_JOIN_COLUMN!=""))
			{	
				//-- we want the cust_id value when set to be something other than the control Id setup for the wssm instance 
				$_SESSION['customerpkvalue'] = $_SESSION['userdb_'._CUST_OC_JOIN_COLUMN];
				$_SESSION['customerpkcolumn'] = _CUST_OC_JOIN_COLUMN;
			}
			else
			{
				//-- we want the cust_id value when set to be the control ID setup for the wssm instance 
				$_SESSION['customerpkvalue'] = $_SESSION['userdb_'.strToLower($_SESSION['config_ac_id'])];
				$_SESSION['customerpkcolumn'] = $_SESSION['config_ac_id'];
			}
			//--


			break;
		default:
			$strResult=$strLoginResult;
	}//end switch/case on session


//-- echo out the result
echo $strResult;
?>
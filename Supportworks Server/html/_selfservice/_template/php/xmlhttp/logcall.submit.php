<?php 	include("ITSMF/xmlmc/common.php");
	//-- This script will loop through posted vars find all those with
	//-- opencall_ and updatedb_

	$prefix = 'wssmlc_';

	//-- check if key matches
	if(!check_secure_key($prefix.'key'))
	{
		?>
		<div class="boxWrapper" style="margin:0px auto 10px auto; width:600px"><img src="img/structure/box_header_left.gif" width="6" height="11" alt="" border="0" /><div class="boxMiddle">
			<div class="boxContent">
				<div class="spacer">&nbsp;</div>
				<!-- box content -->
				<h2>Authentication error</h2>
				<p>There has been an authentication error when attempting to log the request. Please contact your Supportworks Administrator</p>

				<!-- end of box content -->
				<div class="spacer">&nbsp;</div>
			</div>
		</div>
		<div class="boxFooter"><img src="img/structure/box_footer_left.gif" /></div>
		</div>
			<?php 			unset($_SESSION[$prefix.'key']);
			exit;
	}	

	//-- if required opencall fields (like priority) are not passed in will use session default as defined in self serv config
	$arrOpencallValues = Array();
	$arrUpdatedbValues = Array();
	$arrExtendedTables = Array();

	//-- loop through posted vars and id each question
	foreach ($_REQUEST as $key => $val)
	{
		//-- check if we need to split data
		if(strpos($val,ANSWER_SPLIT)!==false)$val = str_replace(ANSWER_SPLIT," and ",$val);

		//echo $key . " : " . $val . "<br/>";
		if(strpos($key,"opencall_")!==false)
		{
			$arrInfo = explode("_",$key,2);
			$colName = $arrInfo[1];
			$arrOpencallValues[$colName] = $val;
		}
		else if (strpos($key,"updatedb_")!==false)
		{
			//-- check opencall priority option
			$arrInfo = explode("_",$key,2);
			$colName = $arrInfo[1];
			$arrUpdatedbValues[$colName] = $val;
		}
		else if (strpos($key,"extbl_")===0)
		{
			//-- we want to store info into an extended table (in hnlt element has id="extbl_tablname_.colname"
			$arrInfo = explode("extbl_",$key,2);
			$strTableAndColName = $arrInfo[1];

			$arrTblInfo = explode("__",$strTableAndColName,2);

			$strExtTable = $arrTblInfo[0];
			$strExtCol = $arrTblInfo[1];

			//-- echo $strExtTable . " : " . $strExtCol;

			if(!isset($arrExtendedTables[$strExtTable]))$arrExtendedTables[$strExtTable] = Array();
			$arrExtendedTables[$strExtTable][$strExtCol] = $val;
		}
		else
		{
			//-- something else
		}
	}

	//-- 84506 - set appcode to itsmf
	$arrOpencallValues['appcode'] = "ITSMF";


	//-- CHECK PRIORITY
	//-- if priority is set to [Use Customer SLA] then set it to cust
	if(($arrOpencallValues['priority']=="[Use Customer SLA]")&&($_SESSION['userdb_priority']!=""))
	{
		$arrOpencallValues['priority'] = $_SESSION['userdb_priority'];
	}
	else if($arrOpencallValues['priority']=="")
	{
		//-- priority is empty so set to session priority (if blank use userdb)
		if($_SESSION['config_sla']!="")		
		{
			//-- use config sla
			$arrOpencallValues['priority'] = $_SESSION['config_sla'];
		}
		else
		{
			//-- use userdb
			$arrOpencallValues['priority'] = $_SESSION['userdb_priority'];
		}
	}

    if(is_array($_SESSION['config_callclasses']))
    {
    	if(
          isset($arrOpencallValues['callclass'])
          && trim($arrOpencallValues['callclass']) != ''
          )
    	{
    	    $arrOpencallValues['status']="Logged";	    
    	}
    	// Do we need an else condition to set status? -- wb
    }	
	
	//-- if logstatus= Incoming OR Logged then handle else assume ok
	if($arrOpencallValues['status']=="Incoming")
	{
		$arrOpencallValues['status'] = CS_INCOMMING;
	}
	else if ($arrOpencallValues['status']=="Logged")
	{
		//-- if have an owner set to unaccepted else unassigned
		$useStatus = ($arrOpencallValues['owner']!="")?CS_UNACCEPTED:CS_UNASSIGNED;
		$arrOpencallValues['status'] = $useStatus;
	}


	$arrOCColumns = Array();
	$arrOCColumns['callClass'] = "callclass";
	$arrOCColumns['slaName'] = "priority";
	$arrOCColumns['assetId'] = "equipment";
	$arrOCColumns['costCenter'] = "costcenter";
	$arrOCColumns['probCode'] = "probcode";
	$arrOCColumns['site'] = "site";
//	$arrOCColumns['condition'] = "";
	$arrOCColumns['groupId'] = "suppgroup";
	$arrOCColumns['analystId'] = "owner";
//	$arrOCColumns['timeSpent'] = "";

	$arrUpColumns['updateMessage'] = "updatetxt";
	$arrUpColumns['updateCode'] = "udcode";
	$arrUpColumns['updateSource'] = "udsource";

	//File//


	$xmlmc = new XmlMethodCall();
	if($arrOpencallValues['status']=="Incoming")
	{
		$xmlmc->SetParam("logIncoming",true);
	}
	foreach($arrOCColumns as $colName => $field)
	{
		if(isset($arrOpencallValues[$field]))
		{
			$xmlmc->SetParam($colName,$arrOpencallValues[$field]);
			unset($arrOpencallValues[$field]);
		}
	}
	foreach($arrUpColumns as $colName => $field)
	{
		if(isset($arrUpdatedbValues[$field]))
		{
			$xmlmc->SetParam($colName,$arrUpdatedbValues[$field]);
			unset($arrUpdatedbValues[$field]);
		}
	}

	$strAdditionalCallValues = "";
	$strOCExtra = "";
	foreach($arrTableFields['opencall'] as $colName => $field)
	{
		$strOCExtra .="<".$colName.">".pfx($field['value'])."</".$colName.">";
	}
	unset($arrTableFields['opencall']);
	if($strOCExtra!="")
	{
		$strAdditionalCallValues = "<opencall>".$strOCExtra."</opencall>";
	}
	$strUpdExtra = "";
	foreach($arrTableFields['updatedb'] as $colName => $field)
	{
		$strUpdExtra .="<".$colName.">".pfx($field['value'])."</".$colName.">";
	}
	unset($arrTableFields['opencall']);
	if($strUpdExtra!="")
	{
		$strAdditionalCallValues .= "<updatedb>".$strUpdExtra."</updatedb>";
	}
	
	foreach($arrExtendedTables as $tableName => $arrTable)
	{
		$boolVal = false;
		foreach($arrTable as $colName => $colValue)
		{
			$strAdditionalCallValues .= "<".$tableName.">";
			$strAdditionalCallValues .="<".$colName.">".pfx($colValue)."</".$colName.">";
			$boolVal = true;
		}
		if($boolVal)
		{
			$strAdditionalCallValues .= "</".$tableName.">";
		}
	}

	if($strAdditionalCallValues!="")
	{
			$xmlmc->SetComplexParam("additionalCallValues",$strAdditionalCallValues);
	}
		
	if($xmlmc->Invoke("selfservice","customerLogNewCall", $_SESSION['server_name']))
	{
		$arrDM = $xmlmc->xmlDom->get_elements_by_tagname("params");
		$xmlMD = $arrDM[0];
		if($xmlMD)
		{
			$children = $xmlMD->child_nodes();
			$dTotal = count($children);
			for ($i=0;$i<$dTotal;$i++)
			{
				$colNode = $children[$i];
				if($colNode->node_name()!="#text" && $colNode->node_name()!="#comment")
				{
					$strColName = $colNode->tagname();
					if($strColName=="callref")
					{
						$new_callref = $colNode->get_content();
						$callref_num = $colNode->get_content();
					}
					elseif($strColName=="respondBy")
					{
						$respondbydate =$colNode->get_content();
					}
					elseif($strColName=="fixBy")
					{
						$fixbydate = $colNode->get_content();
					}
				}
			}
		}
	}
	else
	{
		echo $xmlmc->GetLastError();
		exit;
	}
	$swDATA = new CSwLocalDbConnection;
	if(!$swDATA->Connect())
	{
		echo "Failed to create connection to (".swdsn().")";
		exit;
	}
	$oRS = $swDATA->Query("select h_formattedcallref,fixby,respondby from opencall where callref=".PrepareForSql($callref_num),true,true);
	if(!$oRS->eof)
	{
		$new_callref = $oRS->f("h_formattedcallref",false,true);
		$respondbydate = $oRS->f("respondby",false);
		$fixbydate =  $oRS->f("fixby",false);
	}
	//unset($hdConn); //-- nullify

	//- get active incident coutn to show in menu count 
	$conCache  = database_connect("syscache","","");
	$_SESSION['inccount'] = $conCache->GetRecordCount("opencall", "status < 15 and status != 6 and callclass in ('Change Request','Incident') and cust_id = '".PrepareForSql($_SESSION['customerpkvalue'])."'");


?>

<div class="boxWrapper" style="margin:0px auto 10px auto; width:600px"><img src="img/structure/box_header_left.gif" width="6" height="11" alt="" border="0" /><div class="boxMiddle">
	<div class="boxContent">
		<div class="spacer">&nbsp;</div>
		<!-- box content -->
		<h2>Your request has been logged (<?php echo $new_callref;?>)</h2>
		<p>Thank you. Your request has been logged, with a reference of <b><?php echo $new_callref;?></b>. 

		
		<?php 			//--
			//-- only show resp and fix time if call status is logged as when the status is incoming does not apply sla
			if (($intRespByEpoch>0))
			{
		?>
				The support team will aim to respond to you by <?php echo $respondbydate;?> and resolve your request by <?php echo $fixbydate;?>.</p>	
		<?php 			}
		?>


		<p>To assist us in providing you with a quick response to any future enquires you may have regarding this matter, please keep a note of the above reference. If 
		you call our support hotline on <?php echo $_SESSION['config_helpdeskphone']?> to check the status of your request, you will be asked for this reference.</p>

		<!-- end of box content -->
		<div class="spacer">&nbsp;</div>
	</div>
</div>
<div class="boxFooter"><img src="img/structure/box_footer_left.gif" /></div>
</div>


<div class="boxWrapper" style="margin:0px auto 10px auto; width:600px"><img src="img/structure/box_header_left.gif" width="6" height="11" alt="" border="0" /><div class="boxMiddle">
	<div class="boxContent">
		<div class="spacer">&nbsp;</div>
		<!-- box content -->
		<h2>File Attachment</h2>
		<p>If you have any files, such as business documents, log files or screenshots, that will help the support team expedite your request you can attach them using the field below.</p>	
		<iframe style="width:550px;height:90px;" border="0" frameborder="0" src="php/xmlhttp/uploadfile.php?in_callref=<?php echo $callref_num?>" name="if_uploader"></iframe>
		<!-- end of box content -->
		<div class="spacer">&nbsp;</div>
	</div>
</div>
<div class="boxFooter"><img src="img/structure/box_footer_left.gif" /></div>
</div>

<script autoload>
	//-- show new log count in menu item My requests
	var mnuItem = document.getElementById('mi_custreqs');
	if(mnuItem!=null)
	{
		app.setElementText(mnuItem,"My Requests (<?php echo $_SESSION['inccount'];?> Open)");
	}
</script>


<?php 	include("ITSMF/xmlmc/common.php");

	//-- F0101558
	$uploading= gv('in_uploading');
	$strMessage="";
	$prefix = 'wssmcdup_';

	//-- if page has been submitted
	if($uploading=="1")
	{
		//-- check if key matches
		if(!check_secure_key($prefix.'key'))
		{
			//-- set uploading to zero (determines if action is being taken)
			$strMessage = "Authentication failure. The update was not be made to the call.";
			$uploading = 0;
		}	
	}
	$strKey = generate_secure_key($prefix);
	$_SESSION[$prefix.'key'] = $strKey;

	//-- get file info
	$tmp_name = $_FILES['afile']['tmp_name'];
	$file_name = $_FILES['afile']['name'];
	$file_type = $_FILES['afile']['type'];

	//-- check if uploading
	$update_callref = gv('in_callref');
	$update_authorid = gv('in_authorid');
	$update_desc = gv('in_description');

	$cust_rating =gv('in_crating');
	$cust_origrating =gv('in_oldcrating');
	$cust_origratingtxt =gv('in_oldcratingtxt');	
	$cust_ratingtxt =gv('in_cratingtxt');

	if(!regex_match("/^[0-9]*$/",$update_callref))
	{
		$strMessage = "A submitted variable was identified as a possible security threat.<br> 
						Please contact your system Administrator.";
		$uploading=="0";
	}

	if(($update_callref!="")&&($uploading=="1"))
	{
		//-- determine the update action
		$ud_action =($file_name=="")?"General Update":"Attached File";

	
		//-- rating change
		$strRatingInfo="";
		if($cust_origrating!=$cust_rating && $cust_rating!="")
		{
			//-- changed rating
			$strRatingText = "Neutral";
			switch($cust_rating)
			{
				case 1:
					$strRatingText = "Positive";
					break;
				case 2:
					$strRatingText = "Neutral";
					break;
				case 3:
					$strRatingText = "Negative";
					break;
			}

			$ud_action = "Rating Change";
			$strRatingInfo = "The customer rating for this request was changed to ".$strRatingText.".";
		}

		//-- rating feedback change
		if($cust_origratingtxt!=$cust_ratingtxt)
		{
			$strRatingInfo .= "The customer left the following comment in regards to the rating of this request:\n[". $cust_ratingtxt."]";
		}

		//-- add rating info to desc
		if($update_desc!="")$update_desc .="\n\n";
		$update_desc .=$strRatingInfo;


		if($update_desc=="")
		{
			$strMessage = "Please provide an update description before trying to update this request.";
		}
		else
		{
			//-- create helpdesk session
			$hdConn = new CWSSMActions;
			if(!$hdConn->StartCallUpdate($update_callref, $update_desc,5,"Customer (".$_SESSION['customerid'].")",$ud_action,"1"))
			{
				$strMessage = $hdConn->LastError;
			}
			else
			{
				//-- load call details
				$connCache = new CSwLocalDbConnection;
				//$connCache->Connect("sw_systemdb",swcuid(),swcpwd());
				$connCache->Connect("sw_systemdb","","");
				//-- load call details
				//-- get from syscache or swdata depending on status
				$rsOpencall=$connCache->query("select * from opencall where callref = ".PrepareForSql($update_callref), true, true);
				if($rsOpencall->eof)
				{
					$connSWDATA = new CSwDbConnection;
					$connSWDATA->SwDataConnect();
					$rsOpencall=$connSWDATA->query("select * from opencall where callref = ".PrepareForSql($update_callref), true, true);
					if($rsOpencall==false)
					{
						//$strMessage = "The call data could not be loaded for ".swcallref_str($update_callref).". Please contact your Supportworks administrator.";			
						$strMessage = "The call data could not be loaded for ".htmlentities($update_callref,ENT_QUOTES,'UTF-8').". Please contact your Supportworks administrator.";			
					}
				}

				if($file_name!="")$hdConn->sendfile($tmp_name,$file_name,$file_type);
				$res= $hdConn->CommitCallAction("customerUpdateCall");
				if($res)
				{
					//-- if we have made a rating
					if($cust_rating!="")
					{
						$hdConn->StartCallValuesUpdate($update_callref);
						//-- send c rating
						$hdConn->sendcomplextype("opencall","c_rating",$cust_rating);
						$hdConn->sendcomplextype("opencall","c_ratingtxt",$cust_ratingtxt);
						$hdConn->CommitCallAction("customerUpdateCallValues");
					}								

					$strMessage = "The update was applied to the following request ".($rsOpencall->xf("callref",true)).".";
					//$strMessage = "The update was applied to the following request ".swcallref_str($update_callref).".";
					//$strMessage .= "<script> alert('The update was applied to the following request ".swcallref_str($update_callref).".');</script>";
					//$strMessage .= "<script> alert('The update was applied to the following request ".($rsOpencall->xf("callref",true)).".');</script>";
				}
				else
				{
					//$strMessage = "The update could not be applied to the following request ".swcallref_str($update_callref);
					$strMessage = "The update could not be applied to the following request ".htmlentities($update_callref,ENT_QUOTES,'UTF-8');
				}
			}//--start hd update

		}//--update desc !=""
	}
	else if($update_callref=="")
	{
		$strMessage = "The update could not be made to the call as a call reference was not provided.";
	}

	$connCache = new CSwLocalDbConnection;
	//$connCache->Connect("sw_systemdb",swcuid(),swcpwd());
	$connCache->Connect("sw_systemdb","","");
	//-- load call details
	//-- get from syscache or swdata depending on status
	$rsOpencall=$connCache->query("select * from opencall where callref = ".PrepareForSql($update_callref), true, true);
	if($rsOpencall->eof)
	{
		$connSWDATA = new CSwDbConnection;
		$connSWDATA->SwDataConnect();
		$rsOpencall=$connSWDATA->query("select * from opencall where callref = ".PrepareForSql($update_callref), true, true);
		if($rsOpencall==false)
		{
			//$strMessage = "The call data could not be loaded for ".swcallref_str($update_callref).". Please contact your Supportworks administrator.";			
			$strMessage = "The call data could not be loaded for ".htmlentities($update_callref,ENT_QUOTES,'UTF-8').". Please contact your Supportworks administrator.";			
		}
	}

		if($rsOpencall)
		{
			$oc_callclass = $rsOpencall->f("callclass");
			$oc_status = $rsOpencall->xf("status");
			$oc_rating = $rsOpencall->xf("c_rating");
			$oc_ratingtxt = $rsOpencall->xf("c_ratingtxt");

			//-- wording different when resolved
			if(($oc_status==6)&&($oc_rating==0))
			{
				$rating_label = "Please feedback to us how we are doing as we progress this request for you, this will help us better understand your needs.";
			}
			else
			{
				$rating_label = "Please rate the service that you received from us in relation to this request.  Your feedback is important to us because it helps us better understand your needs and enables us to continue to improve the service that we provide to you.";
			}
		}
?>

<html>
<link href="../../css/structure_ss.css" rel="stylesheet" type="text/css" />
<link href="../../css/elements.css" rel="stylesheet" type="text/css" />
<body style="background-color:#ffffff;overflow:hidden;">
	
<?php 
//-- if closed show message
if($oc_status>15)
{
	$strMessage = "This request is currently closed and cannot be updated.<br/>If you need to re-open this request please contact the support desk.";
}
else
{
?>

<form name="fileloader" action="calldiaryupdate.php" target="_self" enctype="multipart/form-data" method="post" style="width:500px;">


<?php 	if($oc_callclass=='Incident'||$oc_callclass=='Change Request')
	{
?>
	<table class="form-dataTable" style="float:left;width:100%;">
		<tbody>
			<tr>
				<td class="dataValue">
					<?php echo $rating_label;?>
				</td>
			</tr>
			<tr>
				<td class="dataValue" align="center">
					<input type="radio" id="cr_pos" name="in_crating" class="radio" value="1" <?php echo ($oc_rating==1)?"checked":""?>><label for="cr_pos">Positive</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="radio" id="cr_avg" name="in_crating" class="radio" value="2" <?php echo ($oc_rating==2)?"checked":""?>><label for="cr_avg">Neutral</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="radio" id="cr_neg" name="in_crating" class="radio" value="3" <?php echo ($oc_rating==3)?"checked":""?>><label for="cr_neg">Negative</label>
				</td>
			</tr>
			<tr>
				<td class="dataValue">
					Please provide any comments you have about the level of service you are receiving.
				</td>
			</tr>
			<tr>
				<td class="dataValue">
					<input type="text" name="in_cratingtxt" onchange="" value="<?php echo $oc_ratingtxt?>" style="width:100%;" maxlength="124"/>
				</td>
			</tr>
		</tbody>
	</table>
<?php 	}
?>
	<!-- update description -->
	<table class="form-dataTable" style="float:left;width:100%;">
		<tbody>
			<tr>
				<td class="dataValue">
					Please provide an update description. The clearer the description the easier it will be for us to support you.
				</td>
			</tr>
			<tr>
				<td class="dataValue">
					<textarea name="in_description" rows=5 style="width:100%;resize:none;"></textarea>
				</td>
			</tr>
		</tbody>
	</table>

	<!-- send file table -->
	<table class="form-dataTable" style="float:left;width:100%;">
		<tbody>
			<tr>
				<td class="dataValue">
					You can attach a file to this request by using the browse button to select the file that you want to send to us.
					The bigger the file the longer it will take for it to upload, so please be patient when uploading large files.
				</td>
			</tr>
			<tr>
				<td class="dataValue">
					<input type="file" name="afile" style="width:100%;"/>
				</td>
			</tr>
		</tbody>
	</table>
		<input type="hidden" name="in_oldcrating" value="<?php echo $oc_rating;?>">
		<input type="hidden" name="in_oldcratingtxt" value="<?php echo $oc_ratingtxt;?>">

		<input type="hidden" name="in_callref" value="<?php echo htmlentities($update_callref,ENT_QUOTES,'UTF-8');?>"/>
		<input type="hidden" name="in_uploading" value="1"/>
		<input type="hidden" id="<?php echo $prefix;?>key" name="<?php echo $prefix;?>key" value="<?php echo $strKey;?>"/>
<center>
<input type="button" class="button" style="width:230px;font-size:16px;" value="Click here to submit this update" onClick="fileloader.submit();">
</center>

<?php }//-- closed
?>
	<table class="form-dataTable" style="float:left;width:100%;">
		<tbody>
			<tr>
				<td class="dataValue">
					<p><span  style="color:#D40000;"><center><?php echo $strMessage;?></center></span></p>
				</td>
			</tr>
		</tbody>
	</table>
</form>
</body>
</html>

<?php
	//-- will do a table update based on cust table
	//-- if fails exits
	include_once("ITSMF/xmlmc/helpers/update.dbrecord.php");

	//-- only get here if above works
	//-- let person know profile update ok
	function profile_updok()
	{
		GLOBAL $customer_session;
		//-- reload session vars for cust details
		$customer_session->LoadCustomerDetails();
		echo "<font style='color:green'>Your profile information was successfully updated.</font>";
	}
?>

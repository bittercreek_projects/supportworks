<?php
	//-- reset a password
	//-- expects old_password, new_password1 and new_password2 then usertable,userpasscol,userkeycol
	//-- open call details
	include_once("ITSMF/xmlmc/common.php");

	$new_password1 = gv('new_password1');
	$new_password2 = gv('new_password2');
	$old_password = gv('old_password');
	$userpasscol = gv('userpasscol');
	$userkeycol = gv('userkeycol');
	$userpasscol = gv('userpasscol');
	$usertable = gv('usertable');
	$userid = gv('userid');


	if($old_password!=$_SESSION['customerpass'])
	{
		echo "<font style='color:red'>The existing password you provided does not match your current password. Please try again.</font>";
		exit;
	}

	if($new_password1!=$new_password2)
	{
		echo "<font style='color:red'>The new password and the confirm password do not match. Please try again.</font>";;
		exit;
	}

	if(strlen($new_password1)>20)
	{
		echo "<font style='color:red'>The new password is greater than 20 characters please try again.</font>";;
		exit;
	}

	if(strlen($new_password2)<6)
	{
		echo "<font style='color:red'>The new password is less than 6 characters please try again.</font>";;
		exit;
	}

	//-- make sure alphanumeric
	$pattern = '/^(?=.*[A-Za-z])(?=.*[0-9])(?!.*[^A-Za-z0-9])(?!.*s)/';
	if(preg_match($pattern, $new_password1, $matches, PREG_OFFSET_CAPTURE))
	{
		//-- ok update userdb table
		$strUpdate = "update ".$usertable." set ".$userpasscol." = '".$new_password1."' where ".$userkeycol." = '" . $userid . "'";
		$swDB = new CSwDbConnection;
		$swDB->SwDataConnect();
		if(!$swDB->query($strUpdate))
		{
			echo "<font style='color:red'>Failed to update your password. Please contact your Supportworks administrator.</font>";;
			exit;
		}
		$swDB->Close();
	}
	else
	{
		echo "<font style='color:red'>The new password is not alpha numeric. Please try again.</font>";;
		exit;	
	}
	echo "<font style='color:green'>Your password has been reset successfully.</font>";;
	$_SESSION['customerpass']=$password1;
?>
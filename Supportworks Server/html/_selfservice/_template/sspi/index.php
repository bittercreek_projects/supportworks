<?php
 
	error_reporting(E_ERROR | E_PARSE );

 	//-- NWJ - 02.04.2007 - Login script. To be called from browser using http request.

	//-- if has a session kill it
	if($_SESSION['customerid']!="")
	{
		session_destroy();
		session_start();
	}

	//-- includes
	include_once("../_ssconfig.php");
	include_once("ITSMF/xmlmc/classCustomerSession.php"); /* change this to point to application versioned file */
	include_once("stdinclude.php");


	//-- Create an instance of our customer session manager
	$customersession = new classCustomerSession;
	$customersession->initialiseCustomerSession(_INSTANCE_NAME, _SERVER_NAME, _INSTANCE_PATH);


	$loginid   = $_REQUEST['loginid'];
	$loginpass =  $_REQUEST['loginpass'];
	if($loginid=="")
	{
		if(isset($_SERVER['REMOTE_USER']))
		{
			$cred = explode("\\",$_SERVER['REMOTE_USER']);
			if (count($cred) == 1) array_unshift($cred, "(no domain info - perhaps SSPIOmitDomain is On)");
			list($domain, $user) = $cred;

			$loginid   =  $user;
			$loginpass = '_SSPI_'.$domain;
		}
	}

	//-- validate customer login
	//$strResult= "OKSSPI";
	$strResult= "1703";
	switch($customersession->CustomerLogin($loginid, $loginpass))
	{
		case LOGIN_FAILED_NO_SERVER:
			//$strResult="Unable to connect to the Supportworks Server. Please contact your system administrator";
			$strResult= "1704";
			break;
		case LOGIN_FAILED_NOT_NOTREGISTERED:
			//$strResult="You are not registered to access this system. Please contact the support helpdesk.";
			$strResult= "1705";
			break;
		case LOGIN_FAILED_NO_NOACCESS:
			//$strResult="you do not have permission to use the service portal. Please contact the support helpdesk.";
			$strResult= "1706";
			break;
		case LOGIN_FAILED_CHECK_CREDENTIALS:
			//$strResult="SSPI authorisation failure. Please contact the support helpdesk.";
			$strResult= "1707";
			break;
		case LOGIN_FAILED_SERVER_ERROR:
			//$strResult="Server or Web configuration error. Contact your system administrator";
			$strResult= "1708";
			break;
		case CUSTOMER_LOGIN_OK:

			//--
			//-- store the install path
			$_SESSION['INSTANCE_INSTALL'] = "_selfservice\\". _INSTANCE_NAME . "\\";
	
			//-- set cookie to remember me
			$remember   = gv('remember');
			if($remember==1)
			{
				//-- store 30 day cookie using userid as array pointer (means cookie can store mulitple user info)
				//--94802
				$strAppend = "";
				if($_SERVER["HTTPS"]=="on")
					$strAppend = "; Secure";
				header( "Set-Cookie: swssusers[".$loginid."]=".$_SESSION['userdb_firstname'] . ' ' .  $_SESSION['userdb_surname']."; Expires=".date('D, d-M-Y H:i:s GMT',time()+60*60*24*30)."; httpOnly; path=/".$strAppend );
			}


			//-- nwj - 05.08.2008 - set customer pk value (for use when getting call list and saving calls) i.e. this is the value to stick into opencall.cust_id
			//-- note this will not always be the control id as setup in the selfservice config tool
			if (defined('_CUST_OC_JOIN_COLUMN')&&(_CUST_OC_JOIN_COLUMN!=""))
			{	
				//-- we want the cust_id value when set to be something other than the control Id setup for the wssm instance 
				$_SESSION['customerpkvalue'] = $_SESSION['userdb_'._CUST_OC_JOIN_COLUMN];
				$_SESSION['customerpkcolumn'] = _CUST_OC_JOIN_COLUMN;
			}
			else
			{
				//-- we want the cust_id value when set to be the control ID setup for the wssm instance 
				$_SESSION['customerpkvalue'] = $_SESSION['userdb_'.strToLower($_SESSION['config_ac_id'])];
				$_SESSION['customerpkcolumn'] = $_SESSION['config_ac_id'];
			}

			//-- so can be used by any server side processing if needed.
			$_SESSION['SSPION'] = true;
			//--
			break;
		default:
			//$strResult="An unexpected problem occured. Please contact your system administrator";
			$strResult= "1709";
	}//end switch/case on session


	//-- redirect based on result

	if($strResult!="1703")
	{
		//-- echo out the result
		//header('Location: ../?errormsg=' . $strResult.$strParam, 303);
		header('Location: ../?errorid=' . $strResult.$strParam, 303);
	}
	else
	{
		$intCallref = $_REQUEST['viewcallref'];
		$params = "";
		if($intCallref!="") $params =	"?viewcallref=" . $intCallref;
		header('Location: ../portal.php' . $params, 303);
	}
	exit;
?>
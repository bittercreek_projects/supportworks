<?php 	include("_ssconfig.php");
	include("ITSMF/xmlmc/common.php");
	include("ITSMF/xmlmc/helpers/portal.navigation.php");
	include("php/portal.access.php"); //-- store any nav access methods
	
if (!function_exists("session_regenerate_id")) {
		swphp_session_regenerate_id();
	} else {
		session_regenerate_id();
	}
	//F94802
	$strAppend = "";
	if($_SERVER["HTTPS"]=="on")
		$strAppend = "; Secure";
	header( "Set-Cookie: PHPSESSID=".session_id()."; httpOnly; path=/".$strAppend );
	
	$_SESSION['_DISPLAY_ERROR'] = _DISPLAY_ERROR;
	
	//- get active incident coutn to show in menu count 
	$conCache  = database_connect("syscache","","");
	$_SESSION['inccount'] = $conCache->GetRecordCount("opencall", "status < 15 and status != 6 and callclass in ('Change Request','Incident') and cust_id = '".PrepareForSql($_SESSION['customerpkvalue'])."'");

	//-- F0099313 - define header logos
	$_WSSM_HEADER_LOGO="img/header/sw-logo-en.png";
	$_WSSM_TITLE= "img/header/title-selfservice.gif";
	@include("customisation/override.php"); //-- any overriding image paths (for logos)

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>Hornbill Customer Self Service (ITSM-Foundations)</title>
<link href="css/elements.css" rel="stylesheet" type="text/css" />
<link href="css/panels.css" rel="stylesheet" type="text/css" />
<link href="css/structure_ss.css" rel="stylesheet" type="text/css" />

<!-- F0099313 - overriding style sheet include -->
<link href="customisation/css/override.css" rel="stylesheet" type="text/css" />

<script>
	var app = top; //-- when call function from popup dforms use app.function to get to root functions below
	var intViewCallref = "<?php echo  $viewcallref;?>";
	var portalroot= "<?php echo docURL();?>";

	var SS_LOGPROFILELEVELS = "<?php echo $_SESSION['config_logprofilelevels'];?>";
	var SS_FORCEKBSEARCH = 	<?php echo ($_SESSION['config_flags']&2)?1:0;?>;

	var sessionid = "<?php echo $_SESSION['sw_sessionid'];?>";

	//-- what action and content stuff we should load
	var strMainContent = "<?php echo gvs('phpcontent');?>";
	var strActionContent = "<?php echo gvs('phpactions');?>";
	var strLinkID = "<?php echo gvs('link');?>";


</script>

<!-- system js -->
<script src="js/system/base64.js"></script>
<script src="js/system/portal.control.js"></script>
<script src="js/system/xmlhttp.control.js"></script>
<script src="js/system/kbase.control.js"></script>
<script src="js/system/datatable.control.js"></script>
<script src="js/system/toolbar.control.js"></script>
<script src="js/system/date.control.js"></script>
<script src="js/system/tab.control.js"></script>
<script src="js/system/custscript.control.js"></script>

<!-- instance specific js -->
<script src="js/datatable.eventfunctions.js"></script>
<script src="js/tab.eventfunctions.js"></script>
<script src="js/portal.selectors.js"></script>

<!-- custom js -->
<script src="js/custom/datatable.functions.js"></script>
<script src="js/custom/tab.functions.js"></script>
<script src="js/custom/general.js"></script>


</head>

<body onload="onload_portal();" >
	<div id="pageArea">

		<!-- PORTAL HEADER -->	
		<div id="topBanner">
			<span id="enterpriseLogo" style="width:280px;height:50px;margin-left:20px;margin-top:15px;"><img src="<?php echo $_WSSM_HEADER_LOGO;?>" width="270" height="50" alt="" border="0" /></span>			
			<span id='titleLogo'><img src="<?php echo $_WSSM_TITLE;?>" width="180" height="35" alt="" border="0" /></span>
			<div id="helpbox">
				<table>
					<tr><td align="right"><a href="Javascript:top.cust_logoff();">logout</a></td></tr>
					<tr><td class="site-title"></br><?php echo $_SESSION['config_sitename'];?></td></tr>
				</table>
			</div>
		</div>

		<!-- LEFT MENU AREA -->
		<div id="navColumn">
			<?php 				//-- php to construct the menu items
				echo create_navigation_menu("xml/menuset.xml");
			?>
		</div>

		<!-- ACTIONS CONTENT COLUMN -->
		<div id="actionsColumn">
			<!-- loaded automatically -->
		</div>


		<!-- MAIN CONTENT COLUMN -->		
		<div id="contentColumn">
			<!-- loaded automatically -->
		</div>

		<!-- div and iframe that can be used to load bespoke content - typically used when dbl clicknig a row to show update form instead of using a new window 
			function hide_inlineframe() will hide the frame
			function show_inlineframe(strURL) will load url into the frame and display around content area
		-->
		<div id="contentColumnIframe">
			<iframe id="inline-frame" name='inline-frame' src="" style="width:90%;" frameborder='0';></iframe>
		</div>

		<!-- PORTAL FOOTER -->	
        <div id="pageFooter">
            <p>Hornbill ITSM - SelfService Desk <a class='footer-logo' href='http://www.hornbill.com' target="_new"></a></p>
        </div>

	</div>



<!-- date picker div - DO NOT REMOVE -->
<!-- date picker div - DO NOT REMOVE -->
<!-- date picker div - DO NOT REMOVE -->
<iframe id="date-picker-shimer" src="javascript:false;" class="calendar-shimer"></iframe>

<div id="date-picker" class="calendar-holder">
	<table class="calendar-bar">
		<tr>
			<td onClick="prev_year()" title="back a year">
				<img src="img/icons/arr_ll.gif"></img>
			</td>
			<td onClick="prev_month()" title="back a month">
				<img src="img/icons/arr_left.gif"></img>
			</td>
			<td width="100%">
				<span id="cal_date"></span>
			</td>
			<td  onClick="next_month()" title="forward a month">
				<img src="img/icons/arr_right.gif"></img>
			</td>
			<td onClick="next_year()" title="forward a year">
				<img src="img/icons/arr_rr.gif"></img>
			</td>
		</tr>
	</table>
	<div id="date-picker-days">
		<table class="calendar-table" onClick="select_day(event)" onmouseover="hover_in_day(event)" onmouseOut="hover_out_day(event)" border=0 cellspacing=0 cellpadding=0>
		</table>
	</div>
</div>
<!-- eof date picker div -->


</body>
</html>
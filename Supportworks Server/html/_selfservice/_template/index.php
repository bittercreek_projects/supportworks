<?php 
	//--
	//-- This function creates the specified directory path using mkdir(). 
	function RecursiveMkdir($path)
	{
		if (!file_exists($path)&&$path!="")
		{
			//-- The directory doesn't exist.  Recurse, passing in the parent directory so that it gets created.
			RecursiveMkdir(dirname($path));
			mkdir($path, 0777);
		}
	}

	//-- NWJ - make sure session save path exists
	$savepath = ini_get('session.save_path');
	if (file_exists($savepath)==false)
	{
		RecursiveMkdir($savepath);
	}
	
	//--
	//-- kill any session for this ip
	error_reporting(E_ERROR | E_PARSE );
	session_start();
	//F94802
	$strAppend = "";
	if($_SERVER["HTTPS"]=="on")
		$strAppend = "; Secure";
	header( "Set-Cookie: PHPSESSID=".session_id()."; httpOnly; path=/".$strAppend );
	session_destroy();


	//-- NWJ - 17.01.2008 - FR54
	//-- get rmemeber mes
	$strRememberMeRows = "";
	if(isset($_COOKIE['swssusers']))
	{
		foreach ($_COOKIE['swssusers'] as $userid => $fullname) 
		{
			$strRememberMeRows .="<tr><td rowspan='2' align='middle'><img src='img/icons/loginuser.png'/></td><td width='100%'>[<img src='img/icons/forgetme.gif' width='8' height='8' alt='forget me' onclick='forget_me(\"$userid\");'/>]&nbsp;<a href='javascript:remember_user(\"$userid\")'>$fullname</a></td></tr><tr><td><input type='password'  style='display:none;' id='tb_rempass_".$userid."' size='35'><button onclick='process_rememberme_signin();' class='btn_signin' id='btn_rempass_".$userid."'>Sign In</button></td></tr>";
		}
	}	

	//-- F0099313 - define header logos
	$_WSSM_HEADER_LOGO="img/header/sw-logo-en.png";
	$_WSSM_TITLE= "img/header/title-selfservice.gif";
	@include("customisation/override.php"); //-- any overriding image paths (for logos)
	//-- TK handle Errors
	$strMsg = "";
	$errorid = htmlentities($_REQUEST['errorid']);
	if ($errorid == "1701")
	{
		$strMsg = "You have logged out successfully";

	}else if ($errorid == "1702")
	{
		$strMsg = "Your Supportworks session has expired or is invalid. Please log on again";
	}else if ($intErrorID == "1703")
	{
		$strErrorMsg = "OKSSPI";
	}else if ($intErrorID == "1704")
	{
		$strErrorMsg = "Unable to connect to the Supportworks Server. Please contact your system administrator";
	}else if ($intErrorID == "1705")
	{
		$strErrorMsg = "You are not registered to access this system. Please contact the support helpdesk.";
	}else if ($intErrorID == "1706")
	{
		$strErrorMsg = "you do not have permission to use the service portal. Please contact the support helpdesk.";
	}else if ($intErrorID == "1707")
	{
		$strErrorMsg = "SSPI authorisation failure. Please contact the support helpdesk.";
	}else if ($intErrorID == "1708")
	{
		$strErrorMsg = "Server or Web configuration error. Contact your system administrator";
	}else if ($intErrorID == "1709")
	{
		$strErrorMsg = "An unexpected problem occured. Please contact your system administrator";
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Hornbill Customer Self Service (ITSM-Foundations)</title>
<link href="css/structure_ss.css" rel="stylesheet" type="text/css" />
<link href="css/elements.css" rel="stylesheet" type="text/css" />
<!-- F0099313 - overriding style sheet include -->
<link href="customisation/css/override.css" rel="stylesheet" type="text/css" />
<script>

	//-- To Skip Login Screen in SSPI (only when there is no error)
	//-- SSPI - if entering first time with no error try authentication?
	var boolSkipLogin = false;
</script>
<script src="js/system/xmlhttp.control.js"></script>
<script src="customisation/js/override.js"></script>

<script>
	var strMsg = "";
		
	if((boolSkipLogin) && (strMsg==""))
	{
		var strParams = "";
		var intViewCallref = "<?php echo htmlentities($viewcallref);?>";
		if(intViewCallref!="")strParams = "?viewcallref=" + intViewCallref;
		location.href="sspi/index.php" + strParams;
	}
	else if(boolSkipLogin)
	{
		if("<?php echo $strErrorMsg?>"=="OKSSPI")
		{
			var strParams = "";
			var intViewCallref = "<?php echo  $viewcallref;?>";
			if(intViewCallref!="")strParams = "?viewcallref=" + intViewCallref;
			location.href="portal.php" + strParams;
		}
	}


	//-- NWJ - get login details
	function process_login()
	{
		var eleID	= document.getElementById("loginID");
		var elePass = document.getElementById("loginPASS");
		if(eleID && elePass && (eleID.value!="" || elePass.value!=""))
		{
			var strUserID   = eleID.value;
			var strPassword = elePass.value;
			process_http_login(strUserID,strPassword);
		}
		else
		{
			//-- assume sspi - go validate sspi credentials
			location.href="sspi/index.php";
		}
	}

	//-- NWJ - process http login
	function process_http_login(strUserID,strPassword)
	{
		//-- NWJ - 17.01.2008 - SW7.4 FR54 - add remember me capability
		var intRemember=0;
		var oCB = document.getElementById("cb_remember");
		if(oCB!=null)
		{
			intRemember = (oCB.checked)?1:0;
		}

		//-- RJC 62508 16.01.2008 URI encoded the password before passing to server side.		
		var strParams = "remember=" + intRemember + "&loginid=" + encodeURIComponent(strUserID) + "&loginpass=" + encodeURIComponent(strPassword);
		var strURL = "php/xmlhttp/customer_login.php?" + strParams;
		var strResult = run_php(strURL,true);
		redirect_to_portal(strResult);
	}

	//--
	//-- login or sspi is ok so goto portal
	function redirect_to_portal(strResult)
	{
			if(strResult.indexOf("OK")==0)
			{
				//-- load portal page
				var strParams = "";
				var intViewCallref = "<?php echo  $viewcallref;?>";
				if(intViewCallref!="")strParams = "?viewcallref=" + intViewCallref;
				location.href="portal.php" + strParams;
			}
			else
			{
				//-- Show error
				var eleTD	= document.getElementById("tdErrorMessage");
				if(eleTD)
				{
					eleTD.innerHTML = strResult;
				}
			}
	}

	function process_login_key(oEle,anEvent)
	{
		if(anEvent.keyCode==13)process_login();
	}

	function popup_passwordrequest()
	{
		var strURL = "content/passwordrequest.php";
		var newWin = window.open(strURL,"pwdreq","scrollbars=no,resizable=no,menubar=no,toolbar=no,height=300,width=600");	
	}


	//--
	//-- functions to help with rememering ids

	//-- userd click on a remembered id - show password and sign in field
	var strCurrentRememberID = "";
	var oCurrentSignInEle = null;
	var oCurrentUserEle = null;
	function remember_user(strUserID)
	{
		var oEle = document.getElementById("tb_rempass_" + strUserID);
		if(oEle)
		{
			var oBtnSigninEle = document.getElementById("btn_rempass_" + strUserID);
			//-- clear and hide last userid clicked on
			if(oCurrentUserEle!=null) 
			{
				oCurrentSignInEle.style.display = "none";
				oCurrentUserEle.style.display = "none";
				oCurrentUserEle.value = "";
			}

			//-- show new userid clicked on
			oBtnSigninEle.style.display="inline";
			oEle.style.display = "inline";
			oEle.focus();
			oCurrentUserEle = oEle;
			oCurrentSignInEle = oBtnSigninEle;
			strCurrentRememberID = strUserID;
		}
	}

	//-- sign in remember me id
	function process_rememberme_signin()
	{
		if(oCurrentUserEle!=null)
		{
			//-- process login
			var oCB = document.getElementById("cb_remember");
			if(oCB!=null)oCB.checked=true;
			process_http_login(strCurrentRememberID,oCurrentUserEle.value);
		}
	}

	//-- forget userid - call xmlhttp to remove from cookie - then refresh page
	function forget_me(strUserID)
	{
			var strURL = "php/xmlhttp/cookie_remove.php?cookie=swssusers[" + strUserID + "]";
			var strResult = run_php(strURL,true);
			if(strResult.indexOf("OK")==0)
			{
				//-- load welcome page
				location.href="index.php";
			}
	}
	//--
	//-- eof remember me functions

	//-- any onload events
	function onload_events()
	{
		//- -auto focus onto login id
		var oEle = document.getElementById("loginID");
		if(oEle)
		{
			oEle.focus();
		}
	}



</script>

</head>

<body onload="onload_events();">

	<div id="pageArea"  style="background-image: none;">

		<!-- banner -->
		<div id="topBanner">
			<!-- new banner logo 19.05.2010 -->
			<span id="enterpriseLogo" style="width:280px;height:50px;margin-left:20px;margin-top:15px;"><img src="<?php echo $_WSSM_HEADER_LOGO;?>" width="270" height="50" alt="" border="0" /></span>
			<span id='titleLogo'><img src="<?php echo $_WSSM_TITLE;?>" width="180" height="35" alt="" border="0" style="vertical-align:top;margin-top:32px;"/></span>
		</div>
			
				
	<table border="0" width="100%">
	<tr>
	<td>

		<div class="boxWrapper" style="margin:50px auto 50px auto; width:400px"><img src="img/structure/box_header_left.gif" width="6" height="11" alt="" border="0" /><div class="boxMiddle">
		<div class="boxContent"><div class="spacer">&nbsp;</div>
				

					<h1>Login</h1>				
						<!-- table to take login details -->
						<table id='logintable' class="logintable">
							<tr>
								<td><strong>Customer ID:</strong></td>
								<td><input type="text" id="loginID" onkeyup="process_login_key(this,event)"size="35"   AUTOCOMPLETE="OFF"/></td>
							</tr>
							<tr>
								<td><strong>Password:</strong></td>
								<td><input type="password" id="loginPASS" onkeyup="process_login_key(this,event)" size="35"   AUTOCOMPLETE="OFF"/></td>
							</tr>
							<tr>
								<td></td>
								<td><input type="checkbox" id="cb_remember" class="q_check" style="position:relative;left:-3px;"> <label for="cb_remember" style="position:relative;left:-4px;top:-2px;">Remember me on this computer</label></td>
							</tr>
						</table>

						<!-- forgotten password - login -->
						<table width="100%">
							<tr>
								<td><a id='ahref_password' href="javascript:popup_passwordrequest();">Forgotten your password?</a></td>
								<td align="right"><input type="button" name="submit" value="Sign In" class="button" onClick="process_login();"/></td>
							</tr>
						</table>
			</div>

			<!-- NWJ - Error messaging -->
			<table width="100%">
				<tr>
					<td id="tdErrorMessage" class="errorMessage"><?php echo$strMsg?></td>
				</tr>
			</table>

		</div>
		<div class="boxFooter"><img src="img/structure/box_footer_left.gif" /></div>
		</div>
	
		</td>
		<?php 			if($strRememberMeRows!="")
			{
		?>
		<td width="35%">

				<!-- table to show remember mes -->
				<table border="0" cellspacing="1" cellpadding="5">
						<tr>
							<td colspan="2">Below is a list of IDs that have been remembered on this computer. If your ID is remembered you can click on it to start the Supportworks SelfService sign in.</td>
						</tr>
					<?php 						echo $strRememberMeRows;
					?>
				</table>
			</td>
		<?php 			}
		?>
		</tr>
		</table>
					
		<!-- footer -->		
        <div id="pageFooter">
            <p>Hornbill ITSM - SelfService Desk <a class='footer-logo' href='http://www.hornbill.com' target="_new"></a></p>
        </div>
	</div>

</body>
</html>

//-- NWJ
//--
//-- Use this js file to add any custom functions you need to handle
//-- when the use clicks on a table. These functions should be referenced in the dtable xml defs
//-- onrowclick and onrowdblclick


//--
//-- load fsc for a row (service avail tab in portal home)
function dtable_load_fsc(aRow)
{
	var strURL = "content/popups/fscdetail.php";
	varKey= aRow.getAttribute("keyvalue");
	strKeyName = aRow.getAttribute("keyvar");

	//-- get ci name from table
	var strCIdesc = get_col_value(aRow,"description");

	strURL += "?"+strKeyName+"=" + varKey + "&cidesc=" + strCIdesc;
	openWin(strURL,"","scrollbars=yes,resizable=no,menubar=no,toolbar=no,height=600,width=800");
}




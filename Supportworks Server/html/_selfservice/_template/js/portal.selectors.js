//-- functions to support selectors i.e. profile code selector , ci selector, date selector


//-- date selector
//-- popup a date selector (div) to input value to a field
var currDateFocus = null;
function get_dateinput(e)
{
	if (!e) var e = window.event; //-- ie
	//-- cancel bubbling
	e.cancelBubble = true;
	if (e.stopPropagation) e.stopPropagation();

	var oEle = getEventSourceElement(e);

	currDateFocus = oEle;
	show_date(e,paste_date);
}

function paste_date(aDate)
{
	hide_date();
	currDateFocus.setAttribute("dbvalue",date_epoch(aDate));
	//alert(currDateFocus.getAttribute("dbvalue"))
	currDateFocus.value = date_ddmmyyyy(aDate);
}

//-- drop down profile code selected - reload list
function dropdown_profile_code_selected(oLB)
{
	//-- get holders
	var oSpan = ge("span_profilecodedesc");
	var oPCholder = ge("opencall.probcode");	

	//-- get values
	var strUseNewValue = oLB.value;
	var strUseNewText = oLB.options[oLB.selectedIndex].text;

	var strCurrentProfileCode = oPCholder.value;
	var strParentCode = oLB.value;
	var intLevel = oLB.getAttribute("currlevel");
	if(intLevel==null)intLevel=0;

	if(strUseNewValue=="gobacklevel")
	{
		//-- we have a level to go back
		if(strCurrentProfileCode.indexOf("-")>0)
		{
			var arrCodes = strCurrentProfileCode.split("-");
			var arrText = oSpan.innerHTML.split("-&gt;");
			strParentCode = arrCodes[arrCodes.length-2];

			//-- set display and value back one
			strUseNewText = "";
			strUseNewValue = "";
			oPCholder.value	= "";
			for(var x=0; x< arrCodes.length-1;x++)
			{
				if(strUseNewValue!="")
				{
					strUseNewText+="->";
					strUseNewValue+="-";
				}
				strUseNewValue += arrCodes[x];				
				strUseNewText  += arrText[x]
			}
			intLevel=x;

			//-- set elemtn values and clear vars
			oSpan.innerHTML = strUseNewText; 
			oPCholder.value  = strUseNewValue;
			strUseNewValue	= "";
			strUseNewText	= "";
			
		}
		else
		{
			intLevel		= 0;
			strParentCode	= "ROOT";
			oPCholder.value	= "";
			oSpan.innerHTML	= "";
			strUseNewValue	= "";
			strUseNewText	= "";
		}	
	}
	else
	{
		intLevel++;
	}

	oLB.setAttribute("currlevel",intLevel);

	//-- set profile code text
	if(strUseNewText!="")
	{
		var strCurrentText = oSpan.innerHTML;
		if(strCurrentText!="") strCurrentText += "->";
		strCurrentText += strUseNewText;
		oSpan.innerHTML = strCurrentText;
	}
	//-- store value
	if(strUseNewValue!="")
	{
		if (oPCholder.value!="") oPCholder.value += "-";
		oPCholder.value += strUseNewValue
	}

	//-- now get next level of items
	oLB.options.length=1;
	if(oPCholder.value!="")oLB.options[oLB.options.length++] = new Option("<-- Go back a level","gobacklevel");

	if(SS_LOGPROFILELEVELS==intLevel) return;

	
	
	var strURL = "php/xmlhttp/get_profilecodes.php?in_level="  + intLevel + "&in_parentcode=" + strParentCode;
	var strOptions = app.run_php(strURL,true);

	//-- create xml loop to process options
	var oXML = app.create_xml_dom(strOptions);
	if(oXML)
	{
		for(var x=0 ; x < oXML.documentElement.childNodes.length; x++)
		{
			var oOption = oXML.documentElement.childNodes[x];
			var strValue = oOption.getAttribute("value");
			var strText =  getElementText(oOption);

			oLB.options[oLB.options.length++] = new Option(strText,strValue);
		}
	}
}
<?php include("ITSMF/xmlmc/common.php");
include("ITSMF/xmlmc/classKnowledgeBase.php");

//-- Create a new KnowledgeBase access class
$kb = new CSwKnowldgeBaseAccess;

//-- Connect to our knowledgebase
if(!$kb->SwKbCacheConnect())
{
	print "Unable to connect to the Database";
	exit;
}

//-- Get the document details
$docref = gv('docref');
if(!$kb->Query("SELECT * FROM kbdocuments where DocRef = '".PrepareForSql($docref)."'"))
{
	print "Unable to query the Database";
	exit;
}
$kb->Fetch("kbase");


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Knowledgebase Document -<?php echo $docref; ?></title>
	<link href="../../css/structure_ss.css" rel="stylesheet" type="text/css" />
	<link href="../../css/panels.css" rel="stylesheet" type="text/css" />
	<link href="../../css/elements.css" rel="stylesheet" type="text/css" />

	<style>
		body
		{
			padding:0px;
			margin:0px;
			min-width: 500px;  		/* MOST BROWSERS (Not IE6) */
			width:auto;
		}
	</style>

	<script>
		function onload_events()
		{
		}
	</script>
</head>

<body onLoad="window.focus();">


<div class="boxWrapper" style="margin:5px 5px 5px 5px; width:auto" ><img src="../../img/structure/box_header_left.gif" width="6" height="11" alt="" border="0" /><div class="boxMiddle">
		<div class="boxContent">
			<div class="spacer">&nbsp;</div>
				<h1>Knowledgebase Document (<?php echo $docref;?>)</h1>
			<div class="spacer">&nbsp;</div>
		</div>	<!-- end of box content -->
	</div>
	<div class="boxFooter"><img src="../../img/structure/box_footer_left.gif" /></div>
</div>

<div class="boxWrapper" style="margin:5px 5px 5px 5px; width:auto;height:auto;" ><img src="../../img/structure/box_header_left.gif" width="6" height="11" alt="" border="0" /><div class="boxMiddle">
		<div class="boxContent" style="height:100%;">
			<div class="spacer">&nbsp;</div>

				<table width="100%" height="100%">
				  <tr>
					<td class="right" noWrap><b>Document Date:</b></td>
					<td width="100%"><?php echo substr($kbase_docdate,0,10); ?></td>
				  </tr>
				  <tr>
					<td class="right" noWrap><b>Title:</b></td>
					<td><?php echo $kbase_title; ?></td>
				  </tr>
				  <!-- Dans fix to show the doc name when it is an external document-->
				  <?php 
					if($kbase_sourcepath)
					{
					?>
					  <tr>
						<td class="right" noWrap><b>Document Name :</b></td>
						<td><a href="<?php echo  base64_decode(gv('kbdoc'))?>"><?php echo $kbase_sourcepath; ?></a></td>
					  </tr>
					<?php 					}//-- path
					?>
				  <tr>
					<td class="right"><b>Author :</b></td>
					<td><?php echo $kbase_author; ?></td>
				  </tr>
				  <?php 
					if($kbase_callref)
					{
					?>
					  <tr>
						<td class="right" noWrap><b>Request Reference :</b></td>
						<td><?php echo $kbase_callref; ?></td>
					  </tr>
				   <?php 					}//end if callref
					?>
				  <tr>
					<td style="height:10px;"></td><td></td>
  				  </tr>
				  <tr>
					 <td valign="top" class="right"><b>Problem :</b></td>
					<td valign="top"><textarea style="width:100%;height:150px;" readonly><?php echo $kbase_problem; ?></textarea></td>
				   </tr>
				  <tr>
					<td valign="top" class="right"><b>Solution :</b></td>
					<td valign="top"><textarea style="width:100%;height:150px;" readonly><?php echo $kbase_solution; ?></textarea></td>
				   </tr>
				  <?php 
				if ($kbase_keywords){
					?>
				  <tr>
					<td class="right"><b>Keywords :</b></td>
					<td><?php echo $kbase_keywords; ?></td>
				  </tr>
				  <?php 				}//end if keywords

					?>
				</table>
				</br></br>
			<div class="spacer">&nbsp;</div>
		</div>	<!-- end of box content -->
	</div>
	<div class="boxFooter"><img src="../../img/structure/box_footer_left.gif" /></div>
</div>



</body>
</html>

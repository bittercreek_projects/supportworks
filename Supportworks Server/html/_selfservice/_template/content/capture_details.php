<html>

<script>
	function validate_field_input(aForm)
	{
		//-- pass in aForm as can use it to easily access form elements array i.e. aForm.elements[]

		//-- get field and do some validation
		var oTbCallref = document.getElementById('tb_callref');
		if(oTbCallref!=null)		
		{
			if((oTbCallref.value=="") || (isNaN(oTbCallref.value)))
			{
				alert("Please enter a valid call reference");
				return false; //-- stops form from submitting
			}
		}

		//-- allow form to post to server
		return true;
	}
</script>
<body>
	<!-- on submit will run validate_field_input() - if function returns true form will post values to process_update.php which will load into iframe -->
	<form name="update_form" action="process_update.php" method="post" onsubmit="return validate_field_input(this);">
		Callref : <input type="text" id="tb_callref" name="opencall.callref"></br>
		Cust Id : <input type="text" name="opencall.cust_id"></br>
		Charge Centre : <input type="text" name="opencall.costcenter"></br></br>

		Example extended table update : <input type="text" name="extbl_myexttable.somecolname"> (modify to point to your ext table..note if you dont it will fail obviously)</br>


		<input type="submit" value="Submit Request">
	</form>
</body>
</html>
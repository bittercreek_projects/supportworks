<?php
	include_once("ITSMF/xmlmc/common.php");
	include_once("ITSMF/xmlmc/classKnowledgeBase.php");

	if($maxresults == "")$maxresults = 100;
?>
<div class="boxContent">
	<form name="form_kbase_criteria" action="content/knowledgebase_result.php">
        <p>Type of Search:<br/>
			<select id="querytype">
			  <option value="1" <?php if($querytype == 1) echo "selected"; ?>>Natural language query (ask a question)</option>
			  <option value="2" <?php if($querytype == 2) echo "selected"; ?>>Word search (any words can match)</option>
			  <option value="3" <?php if($querytype == 3) echo "selected"; ?>>Word search (all words must match)</option>
			</select>
		</p>
		<p>Your Query or Words to Search For:<br/>
			<textarea id="query" rows="5" cols="40" wrap="on"><?php echo $query; ?></textarea>
		</p>

		<p>Scope of Search:<br/>

			
			<select id="catalog" size="1">
                <?php 
					//-- Get a KB access class and get a list of the catalogs available
					$boolAnalyst = isAnalystPortal();
					$sessionInst = ($boolAnalyst)?$_SESSION['sw_sessionid']:$_SESSION['connector_instance'];

					$kb = new CSwKnowldgeBaseAccess;
					if($kb->ConnectToKbApi($_SESSION['server_name'],$sessionInst,$boolAnalyst))
					{
						//-- Iterate the list and fill the list box
						$kb->ListKnowledgeBaseCatalogs($catalogs);
						while( list($id, $name) = each($catalogs) )
						{
							?>
							<option value="<?php echo $id; ?>" <?php if($id == $catalog) echo "selected"; ?> ><?php echo $name; ?></option>
							<?php
						}

					$kb->CloseKbApi();
					}
				?>
              </select>
		</p>
		<p>Look in:<br/>
			<input type="checkbox" class="q_check" id="so_key"  <?php  echo "checked "; ?>>Document Keywords<br>
			<input type="checkbox" class="q_check" id="so_title" <?php  echo "checked"; ?>>Document Title<br/>
			<input type="checkbox" class="q_check" id="so_probtext" <?php  echo "checked"; ?>>Document Problem Text<br/>
			<input type="checkbox" class="q_check" id="so_soltext" <?php  echo "checked"; ?>>Document Solution Text
		</p>
		<p>
			<input class="input" type="text" id="maxresults" value="<?php echo $maxresults; ?>" size="4">&nbsp;Maximum Number of Results 
 	    </p>
	</form>
	<button onClick="perform_kbase_search(form_kbase_criteria);">Search</button>
</div>


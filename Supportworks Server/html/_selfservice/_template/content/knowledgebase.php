<?php 	include("ITSMF/xmlmc/common.php");

	//-- create a tab control class
	$oTabControl = load_tab_form("knowledgebase");

	$strAppendDesc = "";
	$strTitle = $oTabControl->get_title();
	if($in_fromsr=="1")
	{
		$strTitle = "Log New Request";
		$strAppendDesc = "Before attempting to log a new support request please search the Knowledgebase to see if your problem has a known solution. ";
	}
?>
<div class="boxWrapper" style="margin:0px auto 10px auto;width:95%">
<img src="img/structure/box_header_left.gif" width="6" height="11" alt="" border="0"/><div class="boxMiddle">
<div class="boxContent">
<div class="spacer">&nbsp;</div>
	<!-- box content -->
	<h1><?php echo $strTitle;?></h1>
	
	<p><?php echo $strAppendDesc.nl2br($oTabControl->get_desc());?></p>

	<?php echo $oTabControl->draw('','');?>

	<!-- end of box content -->
	<div class="spacer">&nbsp;</div>
</div>
</div>
<div class="boxFooter"><img src="img/structure/box_footer_left.gif" /></div>
</div>

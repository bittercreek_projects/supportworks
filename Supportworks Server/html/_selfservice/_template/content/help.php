<?php
	session_start();
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="" width="5%">&nbsp;</td>
    <td>
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td colspan="3">Welcome to the Web support Help page. This page will guide you through the usage of the Web helpdesk. If you still have problems using this service, please <a href="#contactinfo" class="text2">contact us directly</a>.<br>
            <br>
            <br>
            <h2>Help Topics</h2>
            <ul>
              <li><a href="#viewproblems" class="text2">Viewing IT issues</a></li>
              <li><a href="#logincident" class="text2">Logging a new request</a></li>
              <li><a href="#viewincidents" class="text2">Viewing your logged requests</a></li>
              <li><a href="#viewsiteincidents" class="text2">Viewing your site's logged requests</a></li>
              <li><a href="#vieworgincidents" class="text2">Viewing your organisation's logged requests</a></li>
              <li><a href="#checkdetails" class="text2">Checking the details of an existing request</a></li>
              <li><a href="#updateincident" class="text2">Updating an existing request</a></li>
              <li><a href="#searchknowledge" class="text2">Searching the Knowledgebase</a></li>
            </ul>
            <br>
            <br>
            <h2>Viewing IT Issues<a name="viewproblems"></a></h2>
            <p>In the &quot;SelfService Noticeboard&quot; box on the right-hand side of the helpdesk home page, there is a summary of (usually widespread) IT issues that may or may not affect you. "Current Problems" would be those issues that have been reported and perhaps not yet investigated. "Known Errors" would be those issues whose root causes are now known. You can view the details of any issue by clicking its title. The details are displayed in a separate pop-up window. Close this window once you have read the details.</p>
            <br>
            <h2>Logging a New Request<a name="logincident"></a></h2>
            <p>You can log a new service request by clicking the &quot;Log New Request&quot; link on the main panel at the left of any page. The Log New Request page is displayed, showing a form with various fields. (Note that you may be required to attempt a Knowledgebase search first, in which case an appropriate <a href="#searchknowledge" class="text2">search</a> form would appear beforehand.)</p>
            <p>On the Log New Request page, if a specific item is giving you trouble, select that asset in the first field. In all cases, select, in the second field, a suitable problem category for the request you are logging, followed by any further sub-categories. In the third field, give a detailed description of your service request. Finally, click the Submit Request link. You should immediately receive a confirmation of your request, complete with a request reference that you should quote in any future communication with the helpdesk.</p>
			<p>At this point, you can submit a file, to be attached to your request, if you feel that this will help the support team solve your problem or otherwise service your request. Simply click the Browse button to help you locate the file concerned and, once the file is specified, click the submission button below.</p>
            <br>
            <h2>Viewing Your Logged Requests<a name="viewincidents"></a></h2>
            <p>You can view a list of the requests you have previously logged by clicking the &quot;My Requests&quot; link on the main panel at the left of any page. The list is displayed on the My Requests page.</p>
            <p>The list initially shows (in the Active Incidents tab) those requests for help that are still in progress and as yet unresolved. You can also display change requests (RFCs) - all non-authorisable, non-rejected active ones, just those awaiting authorisation, or just those that have been rejected. In addition, you can specifically display only on hold, resolved or closed requests (both requests for help and RFCs) by selecting the appropriate tab.</p>
            <p>The column headings at the top of the list have the following meanings:</p>
			<p><b>Rating</b> - This shows a colour-coded representation of the service rating given by you when you updated the request. A green symbol would mean "Positive", a yellow symbol "Neutral" and a red symbol "Negative".</p>
            <p><b>Reference</b> - This shows the request reference, which is the unique identifier for a request.</p>
			<p><b>Type</b> - This indicates the category of a request.</p>
            <p><b>Logged On</b> - This shows the date and time of when you or one of our support analysts logged a request.</p>
            <p><b>Resolved On</b> - This shows the date and time of when, in the estimation of our support analysts, a request was resolved.</p>
			<p><b>Closed On</b> - This shows the date and time of when a request was finally closed.</p>
			<p><b>Resolution Profile</b> - This indicates the resolution category of a resolved or closed request.
            <p><b>Summary</b> - This shows the subject of the request.</p>
            <br>
			<h2>Viewing Your Site's Logged Requests<a name="viewsiteincidents"></a></h2>
			<p>If you can see a &quot;My Site's Requests&quot; link on the main panel at the left, you can click it to view a list of the requests previously logged by others at your site. This page has exactly the same form as the My Requests page.</p>
			<br>
			<h2>Viewing Your Organisation's Logged Requests<a name="vieworgincidents"></a></h2>
			<p>If you can see a &quot;My Organisation's Requests&quot; link on the main panel at the left, you can click it to view a list of the requests previously logged by others in your organisation. This page has exactly the same form as the My Requests page.</p>
			<br>
            <h2>Checking the Details of an Existing Request<a name="checkdetails"></a></h2>
            <p>If you wish to see detailed information on a particular request, you should first display the list of requests as described in <a href="#viewincidents" class="text2">Viewing Your Logged Requests</a> above.  On the My Requests page, double-click that request's entry on the list.  The Request Details page is displayed. On the first tab, various miscellaneous details can be seen. On the Diary tab, you should see a diary of actions relating to the request, carried out either by yourself or by our support analysts.  On the Attachments tab, you would see a list of any files you may have attached to the request.  There is also a fourth tab, by means of which you can <a href="#updateincident" class="text2">update the request</a>.</p>
            <br>
            <h2>Updating an Existing Request<a name="updateincident"></a></h2>
            <p>If you wish to submit further information to the helpdesk about an existing support request, you should first display the details of the request as described in <a href="#checkdetails" class="text2">Checking the Details of an Existing Request</a> above. On the Request Details page, select the Update This Request tab to reveal an update form.</p>
			<p>At the top of that form, you can select a rating for the service you have received on this request so far.
			<p>In the small edit field, you can expand on your rating and type any comments you may have about the service you have received.
            <p>In the large edit field, type the update information you wish to communicate to our support analysts. If you have a file to attach to your update, you can select this via the Browse button. When you have finished, click the submission link. You should immediately receive a confirmation of your update at the bottom of the form.</p>
            <br>
            <h2>Searching the Knowledgebase<a name="searchknowledge"></a></h2>
            <p>The Knowledgebase is a library of useful information, containing FAQs, records of resolved problems and other support documentation.  If you have a problem, you are encouraged (or may even be required) to look through the Knowledgebase before logging a request to see if the problem is a recognised one and has perhaps been previously reported and solved.</p>
            <p>You can conduct a search of the Knowledgebase by clicking the &quot;Knowledgebase Search&quot; link on the left-hand panel of any page, or you may have been automatically diverted to the Knowledgebase when trying to log a request. The Knowledgebase Search page is displayed, where you can enter your search criteria. The criteria available are as follows:</p>
            <p><b>Type of Search</b> - You can select one of three methods by which to search the Knowledgebase. The natural-language query method allows you to ask a question using a normal, properly phrased sentence.  The two word-search methods allow you to specify a string of one or more significant words, of which, respectively, any or all would match what you are looking for.</p>
            <p><b>Your Query or Words to Search For</b> - You should enter your search text in this edit field, in a form that corresponds with the type of search you selected.</p>
            <p><b>Scope of Search</b> - You can select one of several options indicating the area(s) of the Knowledgebase you wish to search. It is possible for you to search the entire Knowledgebase, just the records of resolved problems, just the FAQs, or just topic-based documents.</p>
            <p><b>Look in</b> - If you have selected either of the two word-search methods, you can specify the part(s) of the individual Knowledgebase records in which you want the search process to look for matches. The Document Keywords option relates to the list of words, associated with each record, that provide strong indicators of the content of the record. The Document Title option is self-explanatory. The Document Problem Text option relates to the part of each problem/resolution record that holds the problem description, or the part of each FAQ record that contains the question. Similarly, the Document Solution Text option relates to the part of each problem/resolution record that holds the resolution description, or the part of each FAQ record that contains the answer. (Note that, if you have opted for the natural-language query, all four elements will be included in the search automatically, and any narrower choice of yours will be ignored.)</p>
            <p><b>Maximum Number of Results</b> - The content of this field determines the maximum possible number of results that your search can yield.  Although you can specify an unlimited number, it is worth remembering that the higher it is, the longer you may have to wait. In any case, if you end up with more than 100 results per search, it is likely that your search criteria are too broadly defined.</p>
            <p>When you have entered your search criteria, click the Search button.  The Knowledgebase Results tab is automatically selected, revealing a list of all the results that matched your criteria. Note that you will see a relevance indication for each result only if you used the natural-language query.</p>
            <p>To view the contents of any listed result, click the entry concerned.  A new page is displayed in a separate window, showing the actual FAQ or problem/resolution record, or, for a topic-based entry, a document opens if you have the relevant reader installed on your computer. When you have finished reading the contents of the record or document, just close the window, and you should be able to see the list of results again. Continue examining entries in this way until you have found the information you need. If you are still unable to resolve your problem, you may now wish to <a href="#logincident" class="text2">log a request</a> by clicking the relevant link.</p>
			<br>
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td colspan="3" class="header"><h2>Contact Information<a name="contactinfo"></a></h2></td>
              </tr>
              <tr>
                <td colspan="3" height="10"><img src="images/space.gif" height="1" width="1"></td>
              </tr>
              <tr>
                <td colspan="3"><b><?php echo $_SESSION['config_sitename']; ?></b><br>
                  <br></td>
              </tr>
              <tr>
                <td width="10%" class="form">TELEPHONE</td>
                <td width="17%" nowrap>&nbsp;&nbsp;<?php echo $_SESSION['config_helpdeskphone']; ?></td>
                <td height="15">&nbsp;</td>
              </tr>
              <tr>
                <td height="1" bgcolor="#003366" colspan="2"><img src="images/space.gif" height="1" width="1"></td>
                <td height="1"></td>
              </tr>
              <tr>
                <td class="form">FAX</td>
                <td nowrap>&nbsp;&nbsp;<?php echo $_SESSION['config_helpdeskfax']; ?></td>
                <td height="15">&nbsp;</td>
              </tr>
              <tr>
                <td height="1" bgcolor="#003366" colspan="2"><img src="images/space.gif" height="1" width="1"></td>
                <td height="1"></td>
              </tr>
              <tr>
                <td class="form">URL</td>
                <td nowrap>&nbsp;&nbsp;<a href="<?php echo $_SESSION['config_helpdeskurl']; ?>" class="textblue"><?php echo $_SESSION['config_helpdeskurl']; ?></a></td>
                <td height="15">&nbsp;</td>
              </tr>
              <tr>
                <td height="1" bgcolor="#003366" colspan="2"><img src="images/space.gif" height="1" width="1"></td>
                <td height="1"></td>
              </tr>
              <tr>
                <td class="form">E-MAIL</td>
                <td nowrap>&nbsp;&nbsp;<a href="<?php echo $_SESSION['config_email']; ?>" class="textblue"><?php echo $_SESSION['config_email']; ?></a></td>
                <td height="15">&nbsp;</td>
              </tr>
              <tr>
                <td height="1" bgcolor="#003366" colspan="2"><img src="images/space.gif" height="1" width="1"></td>
                <td height="1"><img src="images/space.gif" height="1" width="1"></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
    <td width="5%">&nbsp;</td>
  </tr>
</table>
</br>
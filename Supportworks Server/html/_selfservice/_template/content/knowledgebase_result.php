<?php
include_once("ITSMF/xmlmc/common.php");
include_once("ITSMF/xmlmc/classKnowledgeBase.php");
	
//-- Create a new KnowledgeBase access class
$kb = new CSwKnowldgeBaseAccess;


$kwflags = 0;
if(gv('so_key'))	$kwflags = $kwflags + KB_QUERYTYPE_FLAG_KEYWORDS;
if(gv('so_title')) 
{
	//echo "boo" . gv('so_title');
	$kwflags = $kwflags + KB_QUERYTYPE_FLAG_TITLE;
}
if(gv('so_probtext')) $kwflags = $kwflags + KB_QUERYTYPE_FLAG_PROBLEM;
if(gv('so_soltext')) $kwflags = $kwflags + KB_QUERYTYPE_FLAG_SOLUTION;


$researchurl = "kbasesearch.php?query=".gv('query')."&querytype=".gv('querytype')."&catalog=".gv('catalog')."&maxresults=".gv('maxresults');

if(gv('so_key')) $researchurl .= "&so_key=1";
if(gv('so_title')) $researchurl .= "&so_title=1";
if(gv('so_probtext')) $researchurl .= "&so_probtext=1";
if(gv('so_soltext')) $researchurl .= "&so_soltext=1";

//-- last param true = ap
if(!$kb->KbQuery(gv('query'), gv('querytype'), gv('catalog'), gv('maxresults'), $kwflags, false))
{
	print '<br><b>Sorry: </b>There has been an unexpected error running your query<br><br>';
	//echo $researchurl;
	foreach ($_REQUEST as $key => $val) print htmlentities($key,ENT_QUOTES,'UTF-8').' = '.htmlentities($val,ENT_QUOTES,'UTF-8').'<br>';
	print '<br>query = '.htmlentities($query,ENT_QUOTES,'UTF-8').'<br>';
	print 'querytype = '.htmlentities($querytype,ENT_QUOTES,'UTF-8').'<br>';
	print 'catalog = '.htmlentities($catalog,ENT_QUOTES,'UTF-8').'<br>';
	print 'maxresults = '.htmlentities($maxresults,ENT_QUOTES,'UTF-8').'<br>';
	print 'flags = '.htmlentities($kwflags,ENT_QUOTES,'UTF-8').'<br>';
	exit;
}//end if query is unsuccessful
else
{
	//-- create table headers
	$strHTML = "<div style='overflow-x:auto;overflow-y:hidden'><table class='dataTable' width='100%'>";
	$strHTML .= "	<thead>";
	$strHTML .= "		<tr>";

	//-- nwj - 30.12.2008 - relevance fix 74736
	if($kb->HasRelevence)
	{
		$strHTML .=	"			<th noWrap>Relevance (%)</th>";
	}

	$strHTML .=	"			<th noWrap>Document Ref.</th>";
	$strHTML .=	"			<th noWrap>Document Title</th>";
	$strHTML .=	"			<th noWrap>Author</th>";
	$strHTML .=	"			<th noWrap>Date Posted</th>";
	$strHTML .= "		</tr>";
	$strHTML .= "	</thead>";
	$strHTML .= "	<tbody>";

	$doccount=0;
	//-- We need a connection onto the helpdesk API to get our relative URL strings
	$boolAnalyst = isAnalystPortal();
	$sessionInst = ($boolAnalyst)?$_SESSION['sw_sessionid']:$_SESSION['connector_instance'];

	if($kb->ConnectToKbApi($_SESSION['server_name'],$sessionInst,$boolAnalyst))
	{
		$max_score = 0;
		while($kb->Fetch("kb"))
		{
			//-- Get our URL for the document
			$kb->GetDocumentURL($kb_docref, $docurl);

			// sandra 10/08/2007 Bug 39972 - check if browser in ssl mode
			if ($_SERVER['HTTPS'] =="on") $docurl=str_replace("http://","https://", $docurl);
			//end sandra

			//-- encode docname and url
			$docurl = base64_encode($docurl);
			$docname = base64_encode($kb_sourcepath);

			//-- nwj - 30.12.2008 - relevance fix 74736
			//--because we are ordering by score we can assume on the first run this wil be the max score.
			if($kb->HasRelevence)
			{
				if($kb_relevance > $max_score){ $max_score = $kb_relevance;} 
				$relperc = @number_format(($kb_relevance/$max_score)*100,2);			
			}

			$strOnClick='app.openWin("content/popups/kbdetail.php?docref='.$kb_docref.'&kbdoc='.$docurl.'&docname='.$docname.'", "theName",  "status=yes,scrollbars=yes,resizable=yes,menubar=no,toolbar=no,height=550,width=800");';
			$strHTML .= "<tr onClick='".$strOnClick."' onmouseover='dtable_row_highlight(this);' onmouseout='dtable_row_lowlight(this);'>";
			
			//-- nwj - 30.12.2008 - relevance fix 74736
			if($kb->HasRelevence) $strHTML .= "<td>".$relperc."</td>";
			
			$strHTML .= "<td>".$kb_docref."</td>";
			$strHTML .= "<td>".$kb_title."</td>";
			$strHTML .= "<td>".$kb_author."</td>";
			$strHTML .= "<td>".$kb_docdate."</td>";
			$strHTML .= "</tr>";
			$doccount++;
		}
		$kb->CloseKbApi();
	}

	$strHTML .=	"		<tr><td colspan='2'>Returned Documents ($doccount)</td><td align='right' colspan='3'><a href='javascript:load_content(\"content/supportcall.php?in_fromkb=1\");'>&lt;-- click here to log a request --&gt;</a></td></tr>";
	$strHTML .= "	</tbody>";
	$strHTML .= "</table></div>";
	echo $strHTML;
}
?>

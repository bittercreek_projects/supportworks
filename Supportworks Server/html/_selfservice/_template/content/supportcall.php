<?php 	//-- Standard Support call. 
	//-- Used for default config

	//-- allows customer to select profile code, asset and provide description and upload files
	include("ITSMF/xmlmc/common.php");

	//-- do we need to force a kb search
	if(($_SESSION['config_flags']&2))
	{
		$in_fromkb = gv('in_fromkb');
		//-- if we havent just come from the kbase search
		if($in_fromkb!="1")
		{
			//-- we want to force the user to do a kb search
			?>
				<script autoload>
					load_content("content/knowledgebase.php?in_fromsr=1");
					//menu_item_selected(document.getElementById('mi_kbase'));
				</script>
			<?php 			exit;
		}
	}
	$prefix = 'wssmlc_';
	$strKey = generate_secure_key($prefix);
	$_SESSION[$prefix.'key'] = $strKey;
	$strDefaultOptions .= "<input type='hidden' id='".$prefix."key' name='".$prefix."key' value='".$strKey."'>";

?>

<!-- header -->
<div class="boxWrapper" style="margin:0px auto 10px auto; width:600px" ><img src="img/structure/box_header_left.gif" width="6" height="11" alt="" border="0" /><div class="boxMiddle">
		<div class="boxContent">
			<div class="spacer">&nbsp;</div>
				<h1>Log New Request</h1>
				<p>All logged requests will be attended to according to your agreed service level. 
				   To assist the support team in providing you with a quick response, please enter a clear description 
				   of your problem and any other relevant information.
				</p>
			<div class="spacer">&nbsp;</div>
		</div>	<!-- end of box content -->
	</div>
	<div class="boxFooter"><img src="img/structure/box_footer_left.gif" /></div>
</div>

<!-- body -->
<div class="boxWrapper" style="margin:0px auto 10px auto; width:600px"><img src="img/structure/box_header_left.gif" width="6" height="11" alt="" border="0" /><div class="boxMiddle">
		<div class="boxContent">
		<div class="spacer">&nbsp;</div>

	<table width="100%">
		<tr>
			<td>
			<div id="page_holder">
			  <form id="logcallform" action="php/xmlhttp/logcall.submit.php">
                <?php if(is_array($_SESSION['config_callclasses'])){ ?>
    			    <p>What type of call do you want to log?</p>
    			    <p>
    			      <select id="opencall.callclass">
    			         <?php     			         foreach($_SESSION['config_callclasses'] as $callclass)
    			         {
    			             if($callclass == $_SESSION['config_callclass'])
    			             {
    			                 print('<option value="'.$callclass.'">');
    			             }
    			             else
    			             {
    			                 print('<option value="'.$callclass.'" selected="selected">');
    			             }

    			             print(htmlentities($callclass, ENT_QUOTES));
    			             print('</option>');
    			         }
    			         ?>
    			      </select>
    			    </p>
                <?php } ?>
			  
				  <p>If you are having a problem with one your work assets please try to identify it from the list below:</p>
				  <p>

					  &nbsp;<select id="opencall.equipment">
							<option value="">-----No Specific Asset-----</option>
							<?php 								$conDb =  database_connect("swdata","","");
								if($conDb->Query("SELECT equipid, manufactur, model FROM equipmnt where owner = '" .PrepareForSQL($_SESSION['customerpkvalue'])."'")) //-- 06.06.2008 - changed to use uderdb_keysearch
								{
									while($conDb->Fetch("asset"))
									{  
									?>
										<option value="<?php echo $asset_equipid; ?>"><?php echo $asset_manufactur . " - " . $asset_model . " (" . $asset_equipid  . ")"; ?> </option>
									<?php 									}//end while records returned
								}//end if query is successful
							?>
						</select>
				  </p>
				  </br>

				<?php 
				if($_SESSION['config_logprofilelevels']>0)
				{
							
				?>
				  <p>Please try and identify the problem area:</p>
				  <p>
					  <span id="span_profilecodedesc"></span>&nbsp;
					  <select id="lb_probcode" currlevel="0" onchange="dropdown_profile_code_selected(this);">
							<option value="">-----Please Select-----</option>
							<?php 								//-- Build query. If we have a parent category we use it and if not, we don't.
								$gl_query = "SELECT * FROM probcode WHERE levelx = 0";
								$gl_query .= " order by code asc";

								if($conDb->Query($gl_query))
								{
									while($conDb->Fetch("pcode"))
									{  
									?>
										<option value="<?php echo $pcode_code; ?>"><?php echo $pcode_descx;?></option>
									<?php 									}//end while records returned
								}//end if query is successful
							?>
						</select>
				  </p>
				  </br>
				  <?php 				  }
				  ?>

				  <p>Please describe the problem you are experiencing:<br>
				  <textarea rows="6" style="width:100%;" id="updatedb.updatetxt"></textarea>
				  </p>
				  <!-- hidden fields that store session vars -->
				  <input type="hidden" id="opencall.probcode" value="">
				  <input type="hidden" id="updatedb.udsource" value="Customer Portal">
				  <input type="hidden" id="updatedb.udcode" value="New Support Request">
				  <input type="hidden" id="opencall.cust_id" value="<?php echo $_SESSION['customerpkvalue']?>">
				  <input type="hidden" id="opencall.fk_company_id" value="<?php echo $_SESSION['userdb_fk_company_id']?>">
				  <input type="hidden" id="opencall.companyname" value="<?php echo $_SESSION['userdb_companyname']?>">
				  <input type="hidden" id="opencall.fk_dept_code" value="<?php echo $_SESSION['userdb_fk_dept_code']?>">

				  <input type="hidden" id="opencall.priority" value="<?php echo $_SESSION['config_sla']?>">
				  <input type="hidden" id="opencall.suppgroup" value="<?php echo $_SESSION['config_suppgroup']?>">
				  <input type="hidden" id="opencall.owner" value="<?php echo $_SESSION['config_owner']?>">
				  <?php if(!is_array($_SESSION['config_callclasses'])){ ?>
				  <input type="hidden" id="opencall.callclass" value="<?php echo $_SESSION['config_callclass']?>">
				  <?php } ?>
				  <input type="hidden" id="opencall.status" value="<?php echo $_SESSION['config_logstatus']?>">
				  <input type='hidden' id='<?php echo $prefix;?>key' name='<?php echo $prefix;?>key' value='<?php echo $strKey;?>'>

              </form>
			</div>
			</td>
		</tr>
	</table>

	<table width="100%">
		<tr>
			<td align="right">
				<input type="button" id="btn_submit" onclick="submit_form('logcallform');" value="Submit Request" class="buttonNext" />
			</td>
		</tr>
	</table>


		<div class="spacer">&nbsp;</div>
		</div><!-- end of box content -->
	</div>
	<div class="boxFooter"><img src="img/structure/box_footer_left.gif" /></div>
</div>
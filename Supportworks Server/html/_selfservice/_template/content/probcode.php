<?php include("ITSMF/xmlmc/classDatabaseAccess.php");

//-- Put ourselves into our Data Dictionary context
$conDb  = new CSwDbConnection;
$conDb->Connect(swdsn(), swuid(), swpwd());


# No level value exists the first time the page is called so here it is set to the default 0. All values
# in this script come from the form below submitting to itself.
if (!$prob_level) $prob_level = 0;

# If the prob_back value is true (set by javascript when user click the back icon) we need to roll back
# the values to work out what the preceeding query was so it can be run again. The level value is decreased.
if ($prob_back)
{
	$prob_level-=2;
	$split_names = explode("#!@%", $prob_names);
	$split_codes = explode("#!@%", $prob_codes);
	$size = sizeof($split_names);
	$prob_select_val = $split_codes[$size-2];
	$prob_names="";
	$prob_codes="";
	for ($x = 0;$x < $size-1;$x++){
		if ($x>0){
			$prob_names.='#!@%'.$split_names[$x];
			$prob_codes.='#!@%'.$split_codes[$x];
			}//end if x > 0
		else {
			$prob_names = $split_names[0];
			$prob_codes = $split_codes[0];
		}//end else x <=0
	}//end for 
}//end if back
# If back is not set then life is easy because we're moving forward, appending the selection name and
# value to the $prob_names & $prob_codes variables is all that is needed.
else 
{
	if ($prob_select_name)
	{
		if ($prob_names) $prob_names .= '#!@%'.$prob_select_name;
		else $prob_names = $prob_select_name;
	}//end if select name
	if ($prob_select_val)
	{
		if ($prob_codes) $prob_codes .= '#!@%'.$prob_select_val;
		else $prob_codes = $prob_select_val;
	}//end if select val
}//end if not back

# Make a copy of the probcode string but in the format the application requires.
$prob_codes_export = str_replace("#!@%","-",$prob_codes);

# Build query. If we have a parent category we use it and if not, we don't.
$gl_query = "SELECT * FROM probcode WHERE levelx = " .$prob_level;
if ($prob_select_val) $gl_query .= " AND parentcode = '" .$prob_select_val. "'";

//-- sandra 10/08/2007 Bug 35679 
$gl_query .= " order by code asc";

# Build the relevent dropdown option box and store it in $prob_box
$count=0;
if($conDb->Query("$gl_query"))
{
	while($conDb->Fetch("probcode"))
	{
		if($count) $prob_box.=",";			
		$count++;
		$prob_box .= '<option value="'.$probcode_code.'">'.$probcode_descx.'</option>';
	}//end while records are returned
	$prob_box = '<select name="prob_select_val" onchange="javascript:nextlevel();"><option value="" selected>----- Please Select -----</option>'.$prob_box.'</select>';
}//end if query is successful
	 
?>
<html>
<script>
	<!--
		function nextlevel(){
			document.ProbCodeForm.prob_select_name.value = document.ProbCodeForm.prob_select_val.options[document.ProbCodeForm.prob_select_val.selectedIndex].text;
			document.ProbCodeForm.submit();
		}//end function nextlevel

		function previouslevel(){
			document.ProbCodeForm.prob_back.value=1;
			document.ProbCodeForm.submit();
		}//end function previouslevel
		//-->
</script>

<body bgcolor="#FFFFFF" topmargin="0" leftmargin="0" rightmargin="0" marginheight="0" marginwidth="0" link="#9f045b" vlink="#9f045b" alink="#9f045b">
<table border="0" cellspacing="0" cellpadding="0">
  <form ID="Form1" method="POST" name="ProbCodeForm">
    <input type="hidden" name="prob_back" value="">
    <input type="hidden" name="prob_level" value="<?php echo $prob_level+1?>">
    <input type="hidden" name="prob_codes" value="<?php echo $prob_codes?>">
    <input type="hidden" name="prob_names" value="<?php echo $prob_names?>">
    <input type="hidden" name="probcode" value="<?php echo $prob_codes_export?>">
    <input type="hidden" name="prob_select_name" value="">
    <tr>
      <?php if ($prob_names) print '<td valign="middle">'.(str_replace("#!@%","&nbsp;/&nbsp;",$prob_names)).'&nbsp;</td>'; 
if (($count>0) && ($prob_level<$config_logprofilelevels)) print '<td valign="middle">'.$prob_box.'</td>';
if ($prob_level>0) print '<td valign="middle">&nbsp;&nbsp;&nbsp;<a href="javascript:previouslevel();"><img src="images/back.gif" width="15" height="10" border="0" alt="Back One Level"></a>&nbsp;&nbsp;&nbsp;</td>';
		?>
      <td><img src="images/space.gif" width="1" height="30"></td>
    </tr>
  </form>
</table>
</body>
</html>

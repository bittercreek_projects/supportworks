<?php 	//-- open call details
	include_once("ITSMF/xmlmc/common.php");

	$in_callref = gv('in_callref');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<title></title>
	<link href="../../css/structure_ss.css" rel="stylesheet" type="text/css" />
	<link href="../../css/panels.css" rel="stylesheet" type="text/css" />
	<link href="../../css/elements.css" rel="stylesheet" type="text/css" />

	<style>
		body
		{
			/* Setting the minumum width of the page 
				995 pixels will fit into a 1024 web browser window */
			min-width: 570px;  		/* MOST BROWSERS (Not IE6) */
			width: expression(document.documentElement.clientWidth < 580 ? "570px" : document.documentElement.clientWidth); /* IE6 */
		}

	</style>

	<script>
		var app = top.app;
		if(opener)
		{
			app = opener.app;
		}

		var info = app.__jsrow_windows[window.name];
		if(info==undefined)
		{
			alert("The window information is not available. This may occur if you have attempted to open this window outside of its parent framework.\n\nPlease contact your Administrator.");
			self.close();
		}

		function onload_events()
		{
			document.title = "Details For <?php echo  swcallref_str($in_callref);?>";

			//-- get window info object
			if(info!=undefined)
			{
				for(strItem in info)
				{
					var oE = document.getElementById(strItem);
					if(oE!=null)
					{
						oE.innerHTML = info[strItem];
					}
				}

				if(info.txt_resolve_text!=undefined)
				{
					//alert(info.txt_resolve_text)
					var oE = document.getElementById("tr_resolve_text");
					if(oE!=null)oE.style.display="";

				}
			}	
		}

	</script>
</head>

<body onload="onload_events();">

<div class="boxWrapper" style="margin:5px 5px 5px 5px;width:563px;height:98%;">
<img src="../../img/structure/box_header_left.gif" width="6" height="11" alt="" border="0"/><div class="boxMiddle">
	<div class="boxContent" style="height:235px;">
			<div class="spacer">&nbsp;</div>
				

				<table border="0">
					<tr>
						<td  align="right">Reference : </td><td> <b><?php echo  swcallref_str($in_callref);?></b></td>
					</tr>
					<tr>
						<td  align="right">Summary : </td><td id='txt_itsm_title'></td>
					</tr>
					<tr>
						<td  align="right">&nbsp;</td><td></td>
					</tr>
					<tr>
						<td align="right">Logged on : </td><td id='txt_logdatex'></td>
					</tr>
					<tr>
						<td  align="right">Resolve by : </td><td id='txt_fixbyx'></td>
					</tr>
					<tr>
						<td  align="right">Impact : </td><td id='txt_itsm_impact_level'><?php echo  $in_itsm_impact_level;?></td>
					</tr>
					<tr>
						<td  align="right">&nbsp;</td><td></td>
					</tr>
					<tr>
						<td  valign='top' align="right">Description : </td><td id='txt_prob_text'><br></td>
					</tr>
					<tr id='tr_resolve_text' style='display:none;'>
						<td  valign='top' align="right" noWrap>Last Update : </td><td id='txt_resolve_text'></td>
					</tr>
				</table>
				<p>

				</p>

			<div class="spacer">&nbsp;</div>
		</div>
	</div>
	<div class="boxFooter"><img src="../../img/structure/box_footer_left.gif" /></div>
</div>

</body>
<?php 
	//-- 13.12.2007 - NWJ
	//-- Displays a single call diary entry
	include_once("ITSMF/xmlmc/common.php");


	


	if ((gv('in_udid') == "") && (gv('IN_UDID') != "")) 
	{
		$udid = gv('IN_UDID');
	}
	else
	{
		$udid = gv('in_udid');
	}

	//-- check if in syscache
	$sysconn = new CSwLocalDbConnection();
	$sysconn->SwCacheConnect();

	//-- try get call from cache
	$sysconn->Query("SELECT callref,updatetxt FROM updatedb where udid = $udid");
	$rsDiary = $sysconn->CreateRecordSet();
	if((!$rsDiary)||($rsDiary->eof))
	{
		//-- failed to get call from cache so get it from swdata
		$swconn = new CSwDbConnection();
		$swconn->Connect(swdsn(), swuid(), swpwd());
		$swconn->Query("SELECT callref,updatetxt FROM updatedb where udid = $udid");
		$rsDiary = $swconn->CreateRecordSet();
		if((!$rsDiary)||($rsDiary->eof))
		{
			//-- call not found ?? in theory should never happen
			?>
			<html>
				<head>
					<meta http-equiv="Pragma" content="no-cache">
					<meta http-equiv="Expires" content="-1">
					<title>Supportworks Call Diary Search Failure</title>
					<script>
						<?php 
							//-- 29.07.2009 - used when we use form post to open a window
							//-- see if we want to resize window
							if((gv('winheight')!="")&&(gv('winwidth')!="")) echo 	"window.resizeTo(".gv('winwidth').",".gv('winheight').")";
						?>
					</script>

				</head>
					<body>
						<br><br>
						<center>
						<p>
							The Supportworks record could not be found<br>
							Please contact your system administrator.
						</p>
						</center>
					</body>
			</html>
			<?php 			exit;
		}
	}
	//callref from diary check
	$in_callref = $rsDiary->f("callref");
	//-- try get call from cache
	$rsCall = $sysconn->Query("SELECT callclass, cust_id, fk_company_id, site FROM opencall where callref = ".PrepareForSql($in_callref),true);
	if((!$rsCall)||($rsCall->eof))
	{
		//-- failed to get call from cache so get it from swdata
		$swconn->Query("SELECT callclass, cust_id, fk_company_id, site FROM opencall where callref = ".PrepareForSql($in_callref));
		$rsCall = $swconn->CreateRecordSet();
		if((!$rsCall)||($rsCall->eof))
		{
			//-- call not found ?? in theory should never happen
			?>
			<html>
				<head>
					<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
					<meta http-equiv="Pragma" content="no-cache">
					<meta http-equiv="Expires" content="-1">
					<title>Supportworks Call Search Failure</title>
				</head>
					<body>
						<br><br>
						<center>
						<p>
							The Supportworks record could not be found<br>
							Please contact your system administrator.
						</p>
						</center>
					</body>
			</html>
			<?php 			exit;
		}
	}

	if($in_callclass=="")
	{
		$in_callclass = $rsCall->f("callclass");
	}

	//--
	//-- if not the customer of this call exit
	if(strtolower($rsCall->f("cust_id"))!=strtolower($_SESSION['customerpkvalue']))
	{

		$strCustomerSites = strtolower(get_customer_sites($_SESSION['customerpkvalue']));
		$strCallSite = "'" . strtolower($rsCall->f("site",true)) ."'";
		$pos = strpos($strCustomerSites, $strCallSite);
		$boolWorksAtSite = ($pos === false)?false:true;
		if($strCallSite=="''")$boolWorksAtSite=false;
		//-- not customers call but can they view org calls?
		if( ($customer_session->IsOption(OPTION_CAN_VIEW_ORGCALLS)==true) && ( strtolower($_SESSION['userdb_fk_company_id']) == strtolower($rsCall->f("fk_company_id")) ) )
		{
			//-- ok to view as same org
		}
		else if( ($customer_session->IsOption(OPTION_CAN_VIEW_SITECALLS)==true) && ($boolWorksAtSite))
		{
			//-- ok to view as works at same site				
		}
		else
		{
			//-- not allowed to view call
			?>
			<html>
				<head>
					<title>Supportworks Security</title>
				</head>
					<body>
						<br><br>
						<center>
						<p>
							You are not the primary customer for this <?php echo $rsCall->xf("callclass");?> and therefore are not allowed to access it.<br>
							Please contact your system administrator.
						</p>
						</center>
					</body>
			</html>
			<?php 			exit;
		}
	}
?>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Call Action Diary for <?php echo  swdti_formatvalue("opencall.callref",$rsDiary->xf('callref'));?></title>
<script>
	<?php 
		//-- 29.07.2009 - used when we use form post to open a window
		//-- see if we want to resize window
		if((gv('winheight')!="")&&(gv('winwidth')!="")) echo 	"window.resizeTo(".gv('winwidth').",".gv('winheight').")";
	?>
</script>

<style>

body
{
	overflow:hidden;
	padding:0px;
	margin:0px;
}
textarea
{
	width:100%;
	height:100%;
	background-color:#efefef;
}
</style>
<body>
<textarea readOnly><?php echo  $rsDiary->xf('updatetxt')?></textarea>
</body>
</html>
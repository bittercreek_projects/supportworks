<?php include_once("itsmf/xmlmc/common.php");

//-- Check For Encrypted URL
if($_REQUEST['in_callref'])
{
	$prefix = 'wssmcdup_';
	//-- check if key matches
	if(!check_secure_key($prefix.'key'))
	{	
		$StrError = true;
	}
	$StrError = false;
}else
{
	if($_REQUEST['ied'])
	{
		//-- Check if anything is in URL unencrypted
		if(($_REQUEST['cust1'])||($_REQUEST['cust1']))
		{			
			$StrError = true;
		}else
		{
			include("../../php/swDecoder.php");//-- Used to Decode URL
			$StrError = false;
		}
	}else
	{
		$StrError = true;
	}
}
if($StrError == true)
{
	//-- IF Error due to no Encypted URL or someone trying to put something into the URL
	?>
	<html>
		<head>
			<meta http-equiv="Pragma" content="no-cache">
			<meta http-equiv="Expires" content="-1">
			<title>Support-Works Call Search Failure</title>
				<link rel="stylesheet" href="sheets/maincss.css" type="text/css">
		</head>
			<body>
				<br></br>
				<center>
				<span class="error">
					A submitted variable was identified as a possible security threat.<br> 
					Please contact your system Administrator.
				</span>
				</center>
			</body>
	</html>
	<?php 	exit;
}

$in_dataid = gv('in_dataid');
$in_callref = gv('in_callref');
$in_filename = gv('in_filename');
	
$path = sw_getcfgstring("Database\\CFAStore")."\\";
$filestore = (sprintf("%08d",$in_callref)).'.'.(sprintf("%03d",$in_dataid));
$dirstore = substr(sprintf("%04d",($in_callref/1000)),0,4);
$filestore = 'f'.$filestore;
$path = $path.$dirstore."\\".$filestore;

	$in_callref = gv('in_callref');
	if(!regex_match("/^[0-9]*$/",$in_callref))
	{
		//-- call not found ?? in theory should never happen
		?>
		<html>
			<head>
				<meta http-equiv="Pragma" content="no-cache">
				<meta http-equiv="Expires" content="-1">
				<title>Support-Works Failure</title>
					<link rel="stylesheet" href="sheets/maincss.css" type="text/css">
			</head>
				<body>
					<br></br>
					<center>
					<span class="error">
						A submitted variable was identified as a possible security threat.<br> 
						Please contact your system Administrator.
					</span>
					</center>
				</body>
		</html>
		<?php 		exit;
	}
	
	//-- create our database connects to swdata and systemdb
	$swconn = new CSwDbConnection();
	$swconn->Connect(swdsn(), swuid(), swpwd());

	$sysconn = new CSwDbConnection();
	$sysconn->SwCacheConnect();
	//-- try get call from cache
	$sysconn->Query("SELECT cust_id, fk_company_id, callclass, site FROM opencall where callref = ".PrepareForSql($in_callref));
	$rsCall = $sysconn->CreateRecordSet();
	if((!$rsCall)||($rsCall->eof))
	{
		//-- failed to get call from cache so get it from swdata
		$swconn->Query("SELECT cust_id, fk_company_id, callclass, site FROM opencall where callref = ".PrepareForSql($in_callref));
		$rsCall = $swconn->CreateRecordSet();
		if((!$rsCall)||($rsCall->eof))
		{
			//-- call not found ?? in theory should never happen
			?>
			<html>
				<head>
					<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
					<meta http-equiv="Pragma" content="no-cache">
					<meta http-equiv="Expires" content="-1">
					<title>Supportworks Call Search Failure</title>
					<script>
						<?php 
							//-- 29.07.2009 - used when we use form post to open a window
							//-- see if we want to resize window
							if((gv('winheight')!="")&&(gv('winwidth')!="")) echo 	"window.resizeTo(".gv('winwidth').",".gv('winheight').")";
						?>
					</script>
				</head>
					<body>
						<br><br>
						<center>
						<p>
							The Supportworks record could not be found<br>
							Please contact your system administrator.
						</p>
						</center>
					</body>
			</html>
			<?php 			exit;
		}
	}
	if(strtolower($rsCall->f("cust_id"))!=strtolower($_SESSION['customerpkvalue']))
	{
		$strCustomerSites = strtolower(get_customer_sites($_SESSION['customerpkvalue']));
		$strCallSite = "'" . strtolower($rsCall->f("site",true)) ."'";
		$pos = strpos($strCustomerSites, $strCallSite);
		$boolWorksAtSite = ($pos === false)?false:true;

		//--
		//-- get calls customers manager id
		if ($rsCall->f("cust_id")!="")
		{
			$strManID = "";
			$strSelectManager = "select fk_manager from userdb where keysearch = '".pfs($rsCall->f("cust_id"))."'";
			$rsMan = $swconn->Query($strSelectManager,true);
			if(($rsMan)&&(!$rsMan->eof))
			{
				$strManID = $rsMan->f('fk_manager');
			}
		}

		//-- not custoemrs call but can they view org calls?
		if( ($customer_session->IsOption(OPTION_CAN_VIEW_ORGCALLS)==true) && (strtolower($_SESSION['userdb_fk_company_id']) == strtolower($rsCall->f("fk_company_id")) ) )
		{
			//-- ok to view as same org
		}
		else if( ($customer_session->IsOption(OPTION_CAN_VIEW_SITECALLS)==true) && ($boolWorksAtSite) )
		{
			//-- ok to view as works at same site
		}
		else if(strtolower($strManID) == strtolower($_SESSION['customerpkvalue']))
		{
			//-- ok to view as customers manager
		}
		else
		{
			//-- not allowed to view call
			?>
			<html>
				<head>
					<title>Supportworks Security</title>
					<script>
						<?php 
							//-- 29.07.2009 - used when we use form post to open a window
							//-- see if we want to resize window
							if((gv('winheight')!="")&&(gv('winwidth')!="")) echo 	"window.resizeTo(".gv('winwidth').",".gv('winheight').")";
						?>
					</script>

				</head>
					<body>
						<br><br>
						<center>
						<p>
							Access to this attachment is denied as you are not the primary customer for the <?php echo $rsCall->xf("callclass");?>.
						</p>
						</center>
					</body>
			</html>
			<?php 			exit;
		}
	}

$url = $in_filename;

$strContentType = "";

//-- if the file is a not html, change the mime type, based on file extension
$fileExtension = substr($url,(strlen($url)-4),4);
switch ($fileExtension) 
{
	case ".doc":
		$strContentType = 'Content-type: application/msword';
	break;
	
	case "acgi":
		$strContentType = 'Content-type: text/html';
	break;
	
	case ".afl":
		$strContentType = 'Content-type: video/animaflex';
	break;
	
	case ".aif":
		$strContentType = 'Content-type: audio/aiff';
	break;

	case "aifc":
		$strContentType = 'Content-type: audio/aiff';
	break;

	case "aiff":
		$strContentType = 'Content-type: audio/aiff';
	break;

	case ".aim":
		$strContentType = 'Content-type: application/x-aim';
	break;

	case ".aps":
		$strContentType = 'Content-type: application/mime';
	break;

	case ".arc":
		$strContentType = 'Content-type: application/octet-stream';
	break;

	case ".art":
		$strContentType = 'Content-type: image/x-jg';
	break;

	case ".asf":
		$strContentType = 'Content-type: video/x-ms-asf';
	break;

	case ".asm":
		$strContentType = 'Content-type: text/x-asm';
	break;

	case ".asp":
		$strContentType = 'Content-type: text/asp';
	break;

	case ".asx":
		$strContentType = 'Content-type: video/x-ms-asf-plugin';
	break;

	case ".avi":
		$strContentType = 'Content-type: video/avi';
	break;

	case ".avs":
		$strContentType = 'Content-type: video/avs-video';
	break;

	case ".bin":
		$strContentType = 'Content-type: application/mac-binary';
	break;

	case ".bmp":
		$strContentType = 'Content-type: image/bmp';
	break;

	case ".boo":
		$strContentType = 'Content-type: application/book';
	break;

	case "book":
		$strContentType = 'Content-type: application/book';
	break;

	case ".boz":
		$strContentType = 'Content-type: application/x-bzip2';
	break;

	case ".bsh":
		$strContentType = 'Content-type: application/x-bsh';
	break;

	case ".bz2":
		$strContentType = 'Content-type: application/x-bzip2';
	break;

	case ".c++":
		$strContentType = 'Content-type: text/plain';
	break;

	case ".cha":
		$strContentType = 'Content-type: application/x-chat';
	break;

	case "chat":
		$strContentType = 'Content-type: application/x-chat';
	break;

	case "lass":
		$strContentType = 'Content-type: application/java';
	break;
	
	case ".com":
		$strContentType = 'Content-type: text/plain';
	break;

	case "conf":
		$strContentType = 'Content-type: text/plain';
	break;

	case ".cpp":
		$strContentType = 'Content-type: text/x-c';
	break;

	case ".cpt":
		$strContentType = 'Content-type: application/x-compactpro';
	break;

	case ".csh":
		$strContentType = 'Content-type: application/x-csh';
	break;

	case ".css":
		$strContentType = 'Content-type: text/css';
	break;

	case ".dir":
		$strContentType = 'Content-type: application/x-director';
	break;

	case ".dot":
		$strContentType = 'Content-type: application/msword';
	break;

	case ".drw":
		$strContentType = 'Content-type: application/drafting';
	break;

	case "dump":
		$strContentType = 'Content-type: application/octet-stream';
	break;

	case ".dvi":
		$strContentType = 'Content-type: application/x-dvi';
	break;

	case ".dwf":
		$strContentType = 'Content-type: model/vnd.dwf';
	break;

	case ".dwg":
		$strContentType = 'Content-type: application/acad';
	break;

	case ".dxr":
		$strContentType = 'Content-type: application/x-director';
	break;

	case ".eps":
		$strContentType = 'Content-type: application/postscript';
	break;

	case ".exe":
		$strContentType = 'Content-type: application/octet-stream';
	break;

	case ".fif":
		$strContentType = 'Content-type: application/fractals';
	break;

	case ".fli":
		$strContentType = 'Content-type: video/fli';
	break;

	case ".for":
		$strContentType = 'Content-type: text/plain';
	break;

	case ".fpx":
		$strContentType = 'Content-type: image/vnd.fpx';
	break;

	case "funk":
		$strContentType = 'Content-type: audio/make';
	break;

	case ".gif":
		$strContentType = 'Content-type: image/gif';
	break;

	case ".gsd":
		$strContentType = 'Content-type: audio/x-gsm';
	break;

	case ".gsm":
		$strContentType = 'Content-type: audio/x-gsm';
	break;

	case "help":
		$strContentType = 'Content-type: application/x-helpfile';
	break;

	case ".hlp":
		$strContentType = 'Content-type: application/x-helpfile';
	break;

	case ".hqx":
		$strContentType = 'Content-type: application/binhex';
	break;

	case ".htm":
		$strContentType = 'Content-type: text/html';
	break;

	case "html":
		$strContentType = 'Content-type: text/html';
	break;

	case ".htt":
		$strContentType = 'Content-type: text/webviewhtml';
	break;

	case ".htx":
		$strContentType = 'Content-type: text/html';
	break;

	case ".ico":
		$strContentType = 'Content-type: image/x-icon';
	break;

	case ".idc":
		$strContentType = 'Content-type: text/plain';
	break;

	case ".ief":
		$strContentType = 'Content-type: image/ief';
	break;

	case "iefs":
		$strContentType = 'Content-type: image/ief';
	break;

	case "imap":
		$strContentType = 'Content-type: x-httpd-imap';
	break;

	case ".isu":
		$strContentType = 'Content-type: video/x-isvideo';
	break;

	case ".jam":
		$strContentType = 'Content-type: audio/x-jam';
	break;

	case ".jav":
		$strContentType = 'Content-type: text/plain';
	break;

	case "java":
		$strContentType = 'Content-type: text/x-java-source';
	break;

	case ".jcm":
		$strContentType = 'Content-type: application/x-java-commerce';
	break;

	case "jfif":
		$strContentType = 'Content-type: image/jpeg';
	break;

	case ".jpe":
		$strContentType = 'Content-type: image/jpeg';
	break;

	case "jpeg":
		$strContentType = 'Content-type: image/jpeg';
	break;

	case ".jpg":
		$strContentType = 'Content-type: image/jpeg';
	break;

	case ".jps":
		$strContentType = 'Content-type: image/x-jps';
	break;

	case ".kar":
		$strContentType = 'Content-type: audio/midi';
	break;

	case ".lam":
		$strContentType = 'Content-type: audio/x-liveaudio';
	break;

	case "list":
		$strContentType = 'Content-type: text/plain';
	break;

	case ".lma":
		$strContentType = 'Content-type: audio/nspaudio';
	break;

	case ".log":
		$strContentType = 'Content-type: text/plain';
	break;

	case ".m1v":
		$strContentType = 'Content-type: video/mpeg';
	break;

	case ".m2a":
		$strContentType = 'Content-type: video/mpeg';
	break;

	case ".m2v":
		$strContentType = 'Content-type: video/mpeg';
	break;

	case ".m3u":
		$strContentType = 'Content-type: audio/x-mpequrl';
	break;

	case ".map":
		$strContentType = 'Content-type: application/x-navimap';
	break;

	case ".mcd":
		$strContentType = 'Content-type: application/mcad';
	break;

	case ".mcf":
		$strContentType = 'Content-type: image/vasa';
	break;

	case ".mid":
		$strContentType = 'Content-type: audio/midi';
	break;

	case "midi":
		$strContentType = 'Content-type: audio/midi';
	break;

	case "mime":
		$strContentType = 'Content-type: www/mime';
	break;

	case "mjpg":
		$strContentType = 'Content-type: video/x-motion-jpeg';
	break;

	case ".mod":
		$strContentType = 'Content-type: audio/mod';
	break;

	case "moov":
		$strContentType = 'Content-type: video/quicktime';
	break;

	case ".mov":
		$strContentType = 'Content-type: video/quicktime';
	break;

	case ".mp2":
		$strContentType = 'Content-type: audio/mpeg';
	break;

	case ".mp3":
		$strContentType = 'Content-type: audio/mpeg3';
	break;

	case ".mpa":
		$strContentType = 'Content-type: audio/mpeg';
	break;

	case ".mpe":
		$strContentType = 'Content-type: video/mpeg';
	break;

	case "mpeg":
		$strContentType = 'Content-type: video/mpeg';
	break;

	case ".mpg":
		$strContentType = 'Content-type: audio/mpeg';
	break;

	case "mpga":
		$strContentType = 'Content-type: audio/mpeg';
	break;

	case ".mpp":
		$strContentType = 'Content-type: application/vnd.ms-project';
	break;

	case ".nif":
		$strContentType = 'Content-type: image/x-niff';
	break;

	case "niff":
		$strContentType = 'Content-type: image/x-niff';
	break;

	case ".oda":
		$strContentType = 'Content-type: application/oda';
	break;

	case ".pas":
		$strContentType = 'Content-type: text/pascal';
	break;

	case ".pbm":
		$strContentType = 'Content-type: image/x-portable-bitmap';
	break;

	case ".pct":
		$strContentType = 'Content-type: image/x-pict';
	break;

	case ".pcx":
		$strContentType = 'Content-type: image/x-pcx';
	break;

	case ".pdf":
		$strContentType = 'Content-type: application/pdf';
	break;

	case ".pgm":
		$strContentType = 'Content-type: image/x-portable-graymap';
	break;

	case ".pic":
		$strContentType = 'Content-type: image/pict';
	break;

	case "pict":
		$strContentType = 'Content-type: image/pict';
	break;

	case ".png":
		$strContentType = 'Content-type: image/png';
	break;

	case ".pot":
		$strContentType = 'Content-type: application/mspowerpoint';
	break;

	case ".ppa":
		$strContentType = 'Content-type: application/vnd.ms-powerpoint';
	break;

	case ".ppm":
		$strContentType = 'Content-type: image/x-portable-pixmap';
	break;

	case ".pps":
		$strContentType = 'Content-type: application/mspowerpoint';
	break;

	case ".ppt":
		$strContentType = 'Content-type: application/mspowerpoint';
	break;

	case ".psd":
		$strContentType = 'Content-type: application/octet-stream';
	break;

	case ".qif":
		$strContentType = 'Content-type: image/x-quicktime';
	break;

	case ".qtc":
		$strContentType = 'Content-type: video/x-qtc';
	break;

	case ".qti":
		$strContentType = 'Content-type: image/x-quicktime';
	break;

	case "qtif":
		$strContentType = 'Content-type: image/x-quicktime';
	break;

	case ".ram":
		$strContentType = 'Content-type: audio/x-pn-realaudio';
	break;

	case ".rgb":
		$strContentType = 'Content-type: image/x-rgb';
	break;

	case ".rmi":
		$strContentType = 'Content-type: audio/mid';
	break;

	case ".rmm":
		$strContentType = 'Content-type: audio/x-pn-realaudio';
	break;

	case ".rmp":
		$strContentType = 'Content-type: audio/x-pn-realaudio';
	break;

	case ".rnx":
		$strContentType = 'Content-type: application/vnd.rn-realplayer';
	break;

	case ".rpm":
		$strContentType = 'Content-type: audio/x-pn-realaudio-plugin';
	break;

	case ".rtf":
		$strContentType = 'Content-type: application/msword';
	break;

	case ".rtx":
		$strContentType = 'Content-type: application/msword';
	break;

	case ".s3m":
		$strContentType = 'Content-type: audio/s3m';
	break;

	case ".sea":
		$strContentType = 'Content-type: application/sea';
	break;

	case "sgml":
		$strContentType = 'Content-type: text/sgml';
	break;

	case ".sit":
		$strContentType = 'Content-type: application/x-stuffit';
	break;

	case ".smi":
		$strContentType = 'Content-type: application/smil';
	break;

	case "smil":
		$strContentType = 'Content-type: application/smil';
	break;

	case ".snd":
		$strContentType = 'Content-type: audio/basic';
	break;

	case ".src":
		$strContentType = 'Content-type: application/x-wais-source';
	break;

	case ".ssi":
		$strContentType = 'Content-type: text/x-server-parsed-html';
	break;

	case ".ssm":
		$strContentType = 'Content-type: application/streamingmedia';
	break;

	case ".svf":
		$strContentType = 'Content-type: image/x-dwg';
	break;

	case ".swf":
		$strContentType = 'Content-type: application/x-shockwave-flash';
	break;

	case "talk":
		$strContentType = 'Content-type: text/x-speech';
	break;

	case ".tar":
		$strContentType = 'Content-type: application/x-tar';
	break;

	case "text":
		$strContentType = 'Content-type: text/plain';
	break;

	case ".tif":
		$strContentType = 'Content-type: image/tiff';
	break;

	case "tiff":
		$strContentType = 'Content-type: image/tiff';
	break;

	case ".tsi":
		$strContentType = 'Content-type: audio/tsp-audio';
	break;

	case ".tsp":
		$strContentType = 'Content-type: audio/tsplayer';
	break;

	case ".tsv":
		$strContentType = 'Content-type: text/tab-saparated-values';
	break;

	case ".txt":
		$strContentType = 'Content-type: text/plain';
	break;

	case ".uue":
		$strContentType = 'Content-type: text-uuencode';
	break;

	case ".vdo":
		$strContentType = 'Content-type: video/vdo';
	break;

	case ".viv":
		$strContentType = 'Content-type: video/vivo';
	break;

	case "vivo":
		$strContentType = 'Content-type: video/vivo';
	break;

	case ".voc":
		$strContentType = 'Content-type: audio/x-voc';
	break;

	case ".vos":
		$strContentType = 'Content-type: video/vosaic';
	break;

	case ".vox":
		$strContentType = 'Content-type: audio/voxware';
	break;

	case "vrml":
		$strContentType = 'Content-type: application/x-vrml';
	break;

	case ".w60":
		$strContentType = 'Content-type: application/wordperfect6.0';
	break;

	case ".w61":
		$strContentType = 'Content-type: application/wordperfect6.1';
	break;

	case "w6w":
		$strContentType = 'Content-type: application/msword';
	break;

	case ".wav":
		$strContentType = 'Content-type: audio/wav';
	break;

	case ".wiz":
		$strContentType = 'Content-type: application/msword';
	break;

	case ".wmf":
		$strContentType = 'Content-type: windows/metafile';
	break;

	case "word":
		$strContentType = 'Content-type: application/msword';
	break;

	case ".wp5":
		$strContentType = 'Content-type: application/wordperfect';
	break;

	case ".wp6":
		$strContentType = 'Content-type: application/wordperfect';
	break;

	case ".wpd":
		$strContentType = 'Content-type: application/wordperfect';
	break;

	case ".wri":
		$strContentType = 'Content-type: application/mswrite';
	break;

	case ".xbm":
		$strContentType = 'Content-type: image/x-xbitmap';
	break;

	case ".xif":
		$strContentType = 'Content-type: image/vnd.xiff';
	break;

	case ".xla":
		$strContentType = 'Content-type: application/excel';
	break;

	case ".xlb":
		$strContentType = 'Content-type: application/excel';
	break;

	case ".xlc":
		$strContentType = 'Content-type: application/excel';
	break;

	case ".xld":
		$strContentType = 'Content-type: application/excel';
	break;

	case ".xlk":
		$strContentType = 'Content-type: application/excel';
	break;

	case ".xll":
		$strContentType = 'Content-type: application/excel';
	break;

	case ".xlm":
		$strContentType = 'Content-type: application/excel';
	break;

	case ".xls":
		$strContentType = 'Content-type: application/vnd.ms-excel';
	break;

	case ".xlt":
		$strContentType = 'Content-type: application/excel';
	break;

	case ".xlv":
		$strContentType = 'Content-type: application/excel';
	break;

	case ".xlw":
		$strContentType = 'Content-type: application/excel';
	break;

	case ".xml":
		$strContentType = 'Content-type: application/xml';
	break;

	case ".xmz":
		$strContentType = 'Content-type: xgl/movie';
	break;

	case ".xpm":
		$strContentType = 'Content-type: image/xpm';
	break;

	case "-png":
		$strContentType = 'Content-type: image/png';
	break;

	case ".zip":
		$strContentType = 'Content-type: application/x-compressed';
	break;

	case "doc":
		$strContentType = 'Content-type: application/msword';
	break;

	case "doc":
		$strContentType = 'Content-type: application/msword';
	break;
}




	//check the few two-letter extensions
	$fileExtension = substr($url,(strlen($url)-3),3);
	switch ($fileExtension) {
		case ".ai":
			$strContentType = 'Content-type: application/postscript';
   		break;
		
		case ".au":
			$strContentType = 'Content-type: audio/basic';
   		break;
		
		case ".bm":
			$strContentType = 'Content-type: image/bmp';
   		break;
		
		case ".bz":
			$strContentType = 'Content-type: application/b-zip';
   		break;
		
		case ".cc":
			$strContentType = 'Content-type: text/plain';
   		break;
		
		case ".dl":
			$strContentType = 'Content-type: video/dl';
   		break;
		
		case ".dv":
			$strContentType = 'Content-type: video/x-dv';
   		break;
		
		case ".gl":
			$strContentType = 'Content-type: video/gl';
   		break;
		
		case ".it":
			$strContentType = 'Content-type: audio/it';
   		break;
		
		case ".js":
			$strContentType = 'Content-type: application/x-javascript';
   		break;
		
		case ".rt":
			$strContentType = 'Content-type: text/richtext';
   		break;
		
		case ".rv":
			$strContentType = 'Content-type: video/vnd.rn-realvideo';
   		break;
		
		case ".ps":
			$strContentType = 'Content-type: application/postscript';
   		break;
		
		case ".qt":
			$strContentType = 'Content-type: video/quicktime';
   		break;
		
		case ".ra":
			$strContentType = 'Content-type: audio/x-pn-realaudio';
   		break;
		
		case ".rf":
			$strContentType = 'Content-type: image/vnd.rn-realflash';
   		break;
		
		case ".rm":
			$strContentType = 'Content-type: application/vnd.rn-realmedia';
   		break;
		
		case ".wp":
			$strContentType = 'Content-type: application/wordperfect';
   		break;
		
		case ".uu":
			$strContentType = 'Content-type: application/octet-stream';
   		break;
		
		case ".xm":
			$strContentType = 'Content-type: audio/xm';
   		break;
		
		case ".xl":
			$strContentType = 'Content-type: application/excel';
   		break;
	}


//-- if no content type set it to binary
if($strContentType=="")$strContentType = 'Content-type: application/octet-stream';

if(function_exists('apache_setenv'))apache_setenv("no-gzip","1");

@ini_set("zlib.output_compression","Off"); # Disable PHP's output compression
ob_end_clean(); # We need to clean all possible output data before reading the file

// Set the correct headers
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header($strContentType);
header('Content-disposition: attachment;filename="'.$in_filename.'"');
$path = preg_replace("/&{1}$/","",$path);
readfile($path);
exit(0);
?>
<?php 	//-- open call details
	include_once("ITSMF/xmlmc/common.php");

	$in_callref = gv('in_callref');
	$in_callclass = gv('in_callclass');

	//-- create our database connects to swdata and systemdb
	$swconn = new CSwDbConnection();
	$swconn->Connect("swdata", "","");//swuid(), swpwd());
//	$swconn->Connect(swdsn(), swuid(), swpwd());

	$sysconn = new CSwLocalDbConnection();
	$sysconn->SwCacheConnect();

	if(!regex_match("/^[0-9]*$/",$in_callref))
	{
		//-- call not found ?? in theory should never happen
		?>
		<html>
			<head>
				<meta http-equiv="Pragma" content="no-cache">
				<meta http-equiv="Expires" content="-1">
				<title>Support-Works Call Search Failure</title>
					<link rel="stylesheet" href="sheets/maincss.css" type="text/css">
			</head>
				<body>
					<br></br>
					<center>
					<span class="error">
						A submitted variable was identified as a possible security threat.<br> 
						Please contact your system Administrator.
					</span>
					</center>
				</body>
		</html>
		<?php 		exit;
	}

	//-- try get call from cache
	$rsCall = $sysconn->Query("SELECT callclass, cust_id, fk_company_id, site FROM opencall where callref = ".PrepareForSql($in_callref),true);
	if((!$rsCall)||($rsCall->eof))
	{
		//-- failed to get call from cache so get it from swdata
		$swconn->Query("SELECT callclass, cust_id, fk_company_id, site FROM opencall where callref = ".PrepareForSql($in_callref));
		$rsCall = $swconn->CreateRecordSet();
		if((!$rsCall)||($rsCall->eof))
		{
			//-- call not found ?? in theory should never happen
			?>
			<html>
				<head>
					<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
					<meta http-equiv="Pragma" content="no-cache">
					<meta http-equiv="Expires" content="-1">
					<title>Supportworks Call Search Failure</title>
				</head>
					<body>
						<br><br>
						<center>
						<p>
							The Supportworks record could not be found<br>
							Please contact your system administrator.
						</p>
						</center>
					</body>
			</html>
			<?php 			exit;
		}
	}

	if($in_callclass=="")
	{
		$in_callclass = $rsCall->f("callclass");
	}

	//--
	//-- if not the customer of this call exit
	if(strtolower($rsCall->f("cust_id"))!=strtolower($_SESSION['customerpkvalue']))
	{

		$strCustomerSites = strtolower(get_customer_sites($_SESSION['customerpkvalue']));
		$strCallSite = "'" . strtolower($rsCall->f("site",true)) ."'";
		$pos = strpos($strCustomerSites, $strCallSite);
		$boolWorksAtSite = ($pos === false)?false:true;
		if($strCallSite=="''")$boolWorksAtSite=false;
		//-- not customers call but can they view org calls?
		if( ($customer_session->IsOption(OPTION_CAN_VIEW_ORGCALLS)==true) && ( strtolower($_SESSION['userdb_fk_company_id']) == strtolower($rsCall->f("fk_company_id")) ) )
		{
			//-- ok to view as same org
		}
		else if( ($customer_session->IsOption(OPTION_CAN_VIEW_SITECALLS)==true) && ($boolWorksAtSite))
		{
			//-- ok to view as works at same site				
		}
		else
		{
			//-- not allowed to view call
			?>
			<html>
				<head>
					<title>Supportworks Security</title>
				</head>
					<body>
						<br><br>
						<center>
						<p>
							You are not the primary customer for this <?php echo $rsCall->xf("callclass");?> and therefore are not allowed to access it.<br>
							Please contact your system administrator.
						</p>
						</center>
					</body>
			</html>
			<?php 			exit;
		}
	}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<title></title>
	<link href="../../css/structure_ss.css" rel="stylesheet" type="text/css" />
	<link href="../../css/panels.css" rel="stylesheet" type="text/css" />
	<link href="../../css/elements.css" rel="stylesheet" type="text/css" />

	<style>
		body
		{
			/* Setting the minumum width of the page 
				995 pixels will fit into a 1024 web browser window */
			min-width: 590px;  		/* MOST BROWSERS (Not IE6) */
			min-height: 560px;  		/* MOST BROWSERS (Not IE6) */
			width: expression(document.documentElement.clientWidth < 590 ? "590px" : document.documentElement.clientWidth); /* IE6 */
			height: 100%;  		/* MOST BROWSERS (Not IE6) */
		}
		html, #tab_holder
		{
			height:95%;
		}
		.boxContent,.boxWrapper,.boxMiddle
		{
			height:100%;
		}
		#tab_contentholder
		{
			height:90%;
		}
	</style>
	<script src="../../js/system/portal.control.js"></script>
	<script src="../../js/system/tab.control.js"></script>
	<script src="../../js/tab.eventfunctions.js"></script>
	<script>
		var app = top.app;
		if(opener)
		{
			app = opener.app;
		}

		function onload_events()
		{
			//-- form is being opened inline with content (not a popup window)
			if(!opener)
			{
				//-- show link to go back to list
				document.getElementById("iframenav").style.display='inline';
			}
		//	document.title = "Request Details For <?php echo ($in_callref);?>";
//			document.title = "Request Details For <?php echo ($in_callref);?>";
		//swcallref_str
		}
	</script>
</head>

<body onload="onload_events();">

<div class="boxWrapper" style="margin:10px 5px 5px 10px;width:98%;">
<img src="../../img/structure/box_header_left.gif" width="6" height="11" alt="" border="0"/><div class="boxMiddle">
<div class="boxContent"><div class="spacer">&nbsp;</div>
	<!-- box content -->
	<p id='iframenav' style='display:none;'><a href="javascript:top.hide_inlineframe();">&lt;&lt;-- Return to list</a></p>

	<?php 		$oTabControl = load_tab_form("calldetail");
	?>
	<h2><?php echo $oTabControl->get_title();?></h2>
	
	<p><?php echo nl2br($oTabControl->get_desc());?></p>

	<?php echo $oTabControl->draw('460px','');?>


	<!-- end of box content -->
	<div class="spacer">&nbsp;</div>
</div>
</div>
<div class="boxFooter"><img src="../../img/structure/box_footer_left.gif" /></div>
</div>

</body>
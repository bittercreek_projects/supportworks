						
<table class="form-dataTable" style="float:left;width:48%">
	<thead>
		<tr>
			<th colspan="2" align='left'>Customer Information</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="dataLabel" style="width:100px;">Customer ID</td>
			<td class="dataValue"><?php echo $rsCust->f('keysearch')?></td>
		</tr>
		<tr>
			<td class="dataLabel">Name</td>
			<td class="dataValue"><?php echo $rsCust->f('firstname') . " " . $rsCust->f('surname')?></td>
			
		</tr>
		<tr>
			<td class="dataLabel">Tel. No.</td>
			<td class="dataValue"><?php echo $rsCust->f('telext')?></td>
			
		</tr>
		<tr>
			<td class="dataLabel">Email</td>
			<td class="dataValue"><?php echo $rsCust->f('email')?></td>
			
		</tr>
		<tr>
			<td class="dataLabel">Site Name</td>
			<td class="dataValue"><?php echo $rsCust->f('site')?></td>
			
		</tr>
		<tr>
			<td class="dataLabel">Customers SLA</td>
			<td class="dataValue"><?php echo $rsCust->f('priority')?></td>
			
		</tr>	
	</tbody>
</table>
<table class="form-dataTable" style="float:right;width:48%">
	<thead>
		<tr>
			<th colspan="2" align='left'>Support Information</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="dataLabel" style="width:100px;">Status</td>
		<!--	<td class="dataValue"><swstatus_str($rsCall->f('status'))?></td>-->
			<td class="dataValue"><?php echo ($rsCall->f('status'))?></td>
		</tr>
		<tr>
			<td class="dataLabel">SLA</td>
			<td class="dataValue"><?php echo $rsCall->f('priority')?></td>
		</tr>
		<tr>
			<td class="dataLabel">Charge Centre</td>
			<td class="dataValue"><?php echo $rsCall->f('costcenter')?></td>
		</tr>
		<tr>
			<td class="dataLabel">Logged By</td>
			<td class="dataValue"><?php echo $rsCall->f('loggedby')?></td>
		</tr>
		<tr>
			<td class="dataLabel">Owned By</td>
			<td class="dataValue"><?php echo $rsCall->f('owner')?></td>
		</tr>
		<tr>
			<td class="dataLabel">Support Group</td>
			<td class="dataValue"><?php echo $rsCall->f('suppgroup');?></td>
		</tr>

	</tbody>
</table>


<table class="form-dataTable" style="width:48%;">
	<thead>
		<tr>
			<th colspan="2" align='left'>Service Level Information</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="dataLabel">Log Date</td>
			<td class="dataValue"><?php echo SwFCTV($rsCall->f('logdatex'));?></td>
		</tr>
		<tr>
			<td class="dataLabel">Respond By</td>
			<td class="dataValue"><?php echo SwFCTV($rsCall->f('respondbyx'));?></td>
		</tr>
		<tr>
			<td class="dataLabel">Fix By</td>
			<td class="dataValue"><?php echo SwFCTV($rsCall->f('fixbyx'))?></td>
		</tr>
		<?php if($rsCall->f('status')==6)
		{
		?>
			<tr>
				<td class="dataLabel">Resolved On</td>
				<td class="dataValue"><?php echo SwFCTV($rsCall->f('closedatex'))?></td>
			</tr>
		<?php 		}

		 if($rsCall->f('status')>15)
		{
		?>
			<tr>
				<td class="dataLabel">Closed On</td>
				<td class="dataValue"><?php echo SwFCTV($rsCall->f('compltdatx'))?></td>
			</tr>
		<?php 		}
		?>
	</tbody>
</table>

<table class="form-dataTable" style="float:left;width:99%;">
	<thead>
		<tr>
			<th colspan="2" align='left'>Problem Area</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="dataValue"><?php echo FormatProblemCode($rsCall->f('probcode'))?>&nbsp;</td>
		</tr>
	</tbody>
</table>

<?php if(($rsCall->f('status')==6)||($rsCall->f('status')>15))
{
?>

<table class="form-dataTable" style="float:left;width:48%;">
	<thead>
		<tr>
			<th colspan="2" align='left'>Resolution Profile</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td  colspan="2" class="dataValue"><?php echo FormatResolutionCode($rsCall->f('fixcode'))?>&nbsp;</td>
		</tr>
	</tbody>
</table>
<?php }
?>

<table class="form-dataTable" style="width:99%;">
	<thead>
		<tr>
			<th colspan="2" align='left'>Summary</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td  colspan="2" class="dataValue"><?php echo $rsCall->f('itsm_title');?></td>
		</tr>
	</tbody>
</table>
<table class="form-dataTable" style="width:99%;">
	<thead>
		<tr>
			<th colspan="2" align='left'>Last Diary Update</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td  colspan="2" class="dataValue"><div style="width:100%;height:80px;overflow:auto;"><?php echo get_call_last_update_text($rsCall->f('callref'),'syscache',10000,1);?></div></td>
		</tr>
	</tbody>
</table>

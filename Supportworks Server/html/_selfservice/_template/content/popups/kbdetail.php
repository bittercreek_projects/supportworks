<?php include("ITSMF/xmlmc/common.php");
include("ITSMF/xmlmc/classKnowledgeBase.php");

//-- Create a new KnowledgeBase access class
$kb = new CSwKnowldgeBaseAccess;

//-- Get the document details
$docref = gv('docref');
if(!$kb->getDocInfo($docref,"kbase"))
{
	print "Unable to query the Database";
	exit;
}

if($kbase_sourcepath!="")
{
	//-- have a document so just load it
	$strLoc = base64_decode(gv('kbdoc'));
	//-- swap out 127.0.0.1 with servers ip
	$strLoc = str_replace('\\',"/",$strLoc);
	header("Location: $strLoc");
	exit;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Knowledgebase Document -<?php echo $docref; ?></title>
	<link href="../../css/structure_ss.css" rel="stylesheet" type="text/css" />
	<link href="../../css/print_override.css" rel="stylesheet" type="text/css" />

	<style>
		body
		{
			padding:0px;
			margin:0px;
			min-width: 500px;  		/* MOST BROWSERS (Not IE6) */
			width:auto;
		}
	</style>

	<script>
		function onload_events()
		{
		}
	</script>
</head>

<body onLoad="window.focus();">

<div id="activepagecontentColumn" >
<!-- left hand col -->

<div id="formArea" style="width:100%;">
<div id="swtPageTop"><img src="img/structure/box_header_left.gif" id="swtImgTopLeft" /></div>
<div id="swtInfoBody">

<div class="sectionHead">
	<table class="sectionTitle">
		<tr>
			<td class="titleCell" noWrap><h1>Knowledgebase Document <?php echo $docref;?></h1></td>
			<td class="endCell"></td>
		</tr>
	</table>	
	<table>
	<tr>
		<td class="right" valign="top">Title :</td><td><span><?php echo $kbase_title; ?></span></td>
		<td class="right" valign="top">Created On :</td><td><span><?php echo substr($kbase_docdate,0,10);?></span></td>
	</tr>
	<tr>
		<td class="right" valign="top">Author :</td><td><span><?php echo $kbase_author; ?></span></td>
		<td class="right" valign="top">Call Reference :</td><td><span><?php echo $kbase_callref; ?></span></td>
	</tr>
	<tr>
		<td class="right" valign="top">Problem Profile :</td><td><span colspan="4"><?php echo $kbase_callprobcode; ?></span></td>
	</tr>
	<tr>
		<td class="right" valign="top">Keywords :</td><td><span colspan="4"><?php echo $kbase_keywords; ?></span></td>
	</tr>
	</table>
</div>

<div class="sectionHead">
	<table class="sectionTitle">
		<tr>
			<td class="titleCell" noWrap><h1>Problem Description</h1></td>
			<td class="endCell"></td>
		</tr>
	</table>	
	<?php echo nl2br($kbase_problem); ?></textarea>
</div>


<div class="sectionHead">
	<table class="sectionTitle">
		<tr>
			<td class="titleCell" noWrap><h1>Solution Description</h1></td>
			<td class="endCell"></td>
		</tr>
	</table>	
	<?php echo embedYoutube(nl2br($kbase_solution)); ?></textarea>
</div>


	</div>
	<div id="swtPageBottom"><img src="img/structure/swt_page_bl.gif"  id="swtImgBottomLeft" /></div>
</div><!-- form area -->


</div> <!-- activepagecontentColumn -->


</body>
</html>

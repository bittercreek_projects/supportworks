<?php
	//-- 20.05.2004 - NWJ
	//-- Called by Supportworks after a call search
	//-- determines which php to show based on call class

	include_once("ITSMF/xmlmc/common.php");

	
	if ( (gv('callref') == "") && (gv('CALLREF') != "") ) 
	{
		$callref = gv('CALLREF');
	}
	else
	{
		$callref = gv('callref'); //-- make local
	}

	//-- create our database connects to swdata and systemdb
	$swconn = new CSwDbConnection();
//	$swconn->Connect(swdsn(), swuid(), swpwd());
	$swconn->Connect("swdata","","");//, swuid(), swpwd());

	$sysconn = new CSwLocalDbConnection();
	$sysconn->SwCacheConnect();
//	$sysconn->LoadDataDictionary("itsmv2");

	if(!regex_match("/^[0-9]*$/",$callref))
	{
		//-- call not found ?? in theory should never happen
		?>
		<html>
			<head>
				<meta http-equiv="Pragma" content="no-cache">
				<meta http-equiv="Expires" content="-1">
				<title>Support-Works Failure</title>
					<link rel="stylesheet" href="sheets/maincss.css" type="text/css">
			</head>
				<body>
					<br></br>
					<center>
					<span class="error">
						A submitted variable was identified as a possible security threat.<br> 
						Please contact your system Administrator.
					</span>
					</center>
				</body>
		</html>
		<?php
		exit;
	}
	
	//-- try get call from cache
	$rsCall = $sysconn->Query("SELECT * FROM opencall where callref = ".PrepareForSql($callref),true);
	if((!$rsCall)||($rsCall->eof))
	{
		//-- failed to get call from cache so get it from swdata
		$swconn->Query("SELECT * FROM opencall where callref = ".PrepareForSql($callref));
		$rsCall = $swconn->CreateRecordSet();
		if((!$rsCall)||($rsCall->eof))
		{
			//-- call not found ?? in theory should never happen
			?>
			<html>
				<head>
					<meta http-equiv="Pragma" content="no-cache">
					<meta http-equiv="Expires" content="-1">
					<title>Support-Works Call Search Failure</title>
				</head>
					<body>
						<br><br>
						<center>
						<p>
							The Supportworks record could not be found<br>
							Please contact your system administrator.
						</p>
						</center>
					</body>
			</html>
			<?php
			exit;
		}
	}

	//-- if cust_id is not empty load userdb record
	if($rsCall->f("cust_id")!="")
	{
		$selcust = "select * from userdb where keysearch = '" . pfs($rsCall->f("cust_id")) . "'";
		$swconn->Query($selcust);
		$rsCust = $swconn->CreateRecordSet();
	}
	if(isset($rsCust)==false)$rsCust = new odbcRecordsetDummy;
	
	
	//-- set up some vars that we will use
	$callclass=$rsCall->f('callclass');
	$callstatus=$rsCall->f('status');
	$cicallcausecode="";
	//--
	//-- depending on callclass include content page ???


	//-- get text we will show on causeing items header
	$citextcode=explode("-",$cicallcausecode);
	$citextcode=(isset($citextcode[1]))?$citextcode[1]:$citextcode[0];
	$citextcode=	ucfirst(strtolower($citextcode));
	
	//-- nwj - just use standard call for now unril noko gives us pages
	$include_content="calldata.php";
?>

<?php include($include_content);?>

<?php 	include_once("ITSMF/xmlmc/common.php");	


	$update_callref=gv("in_callref");
	if($update_callref=="")
	{
		?>
			<script>
				alert("Missing request reference. This form cannot open. please contact your Supportworks administrator.");
				self.close();
			</script>
		<?php 		//exit;
	}

	
	$connSWDATA = new CSwDbConnection;
	$connSWDATA->Connect(swdsn(),swuid(),swpwd());

	$connCache = new CSwLocalDbConnection;
	$connCache->Connect("sw_systemdb",swcuid(),swcpwd());

	if(!regex_match("/^[0-9]*$/",$update_callref))
	{
		//-- call not found ?? in theory should never happen
		?>
		<html>
			<head>
				<meta http-equiv="Pragma" content="no-cache">
				<meta http-equiv="Expires" content="-1">
				<title>Support-Works Call Search Failure</title>
					<link rel="stylesheet" href="sheets/maincss.css" type="text/css">
			</head>
				<body>
					<br></br>
					<center>
					<span class="error">
						A submitted variable was identified as a possible security threat.<br> 
						Please contact your system Administrator.
					</span>
					</center>
				</body>
		</html>
		<?php 		exit;
	}
	
	//-- load call details
	//-- get from syscache or swdata depending on status
	$connCache->query("select * from opencall where callref = ".PrepareForSql($update_callref));
	$rsOpencall=$connCache->CreateRecordSet();
	if($rsOpencall==false)
	{
		$connSWDATA->query("select * from opencall where callref = ".PrepareForSql($update_callref));
		$rsOpencall=$connSWDATA->CreateRecordSet();
		if($rsOpencall==false)
		{
			?>
				<script>
					alert("The opencall record for (<?php echo swcralref_str($update_callref);?>) could not be loaded.");
				</script>
			<?php 			exit;
		}
	}

	//-- if has cust_id get customer details
	if($rsOpencall->f("cust_id")!="")
	{
		$connSWDATA->query("select * from userdb where keysearch = '".$rsOpencall->f("cust_id")."'");
		$rsUserdb=$connSWDATA->CreateRecordSet();
		if($rsUserdb==false)
		{
			?>
				<script>
					alert("The customer record for (<?php echo swcralref_str($update_callref);?>) could not be loaded.");
				</script>
			<?php 			exit;
		}
	}

	//-- load profile desc etc

?>
<table width="100%">
<tr>
	<td>
		<div class="boxWrapper" style="margin:50px auto 10px auto; width:400px" ><img src="img/structure/box_header_left.gif" width="6" height="11" alt="" border="0" /><div class="boxMiddle">
			<div class="boxContent">
				<div class="spacer">&nbsp;</div>
					<h2>Customer Details</h2>
					<p>Customer Name : <input type="text" class="sm_txt" value="<?php echo $rsUserdb->f('fullname')?>"></p>
					<p>Telephone : <input type="text" class="sm_txt" value="<?php echo $rsUserdb->f('telext')?>"> 
					<p>Email : <input type="text" class="sm_txt" value="<?php echo $rsUserdb->f('email')?>"> 
					<p>Service Level : <input type="text" class="sm_txt" value="<?php echo $rsUserdb->f('priority')?>"> 
				<div class="spacer">&nbsp;</div>
			</div>	<!-- end of box content -->
			<div class="boxFooter"><img src="img/structure/box_footer_left.gif" /></div>
		</div>
	</td>
	<td>
		<div class="boxWrapper" style="margin:50px auto 10px auto; width:400px" ><img src="img/structure/box_header_left.gif" width="6" height="11" alt="" border="0" /><div class="boxMiddle">
			<div class="boxContent">
				<div class="spacer">&nbsp;</div>
					<h2>Service Level Information</h2>
					<p>Logged On : <input type="text" class="sm_txt" value="<?php echo SwFCTV($rsOpencall->f('logdatex'));?>"> 
					<p>Service Level : <input type="text" class="sm_txt" value="<?php echo $rsOpencall->f('priority');?>"></p>
					<p>Respond By : <input type="text" class="sm_txt" value="<?php echo SwFCTV($rsOpencall->f('fixbyx'));?>"> 
					<p>Fix By : <input type="text" class="sm_txt" value="<?php echo SwFCTV($rsOpencall->f('respondbyx'));?>"> 
				<div class="spacer">&nbsp;</div>
			</div>	<!-- end of box content -->
			<div class="boxFooter"><img src="img/structure/box_footer_left.gif" /></div>
		</div>

	</td>
</tr>
</table>




<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
<head>
<title>Customer Password Request</title>

<link href="../css/structure_ss.css" rel="stylesheet" type="text/css" />
<link href="../css/elements.css" rel="stylesheet" type="text/css" />


	<style>
	body
	{
		/* Setting the minumum width of the page 
			995 pixels will fit into a 1024 web browser window */
		min-width: 600px;  		/* MOST BROWSERS (Not IE6) */
		width: expression(document.documentElement.clientWidth < 600 ? "600px" : document.documentElement.clientWidth); /* IE6 */
	}
	</style>

</head>

<?php
if($customerid == "")
{
?>

<body>


<div class="boxWrapper" style="margin:20px 20px 20px 20px; width:565px" ><img src="../img/structure/box_header_left.gif" width="6" height="11" alt="" border="0" /><div class="boxMiddle">
		<div class="boxContent">
			<div class="spacer">&nbsp;</div>
				<h1>Password Reminder Request</h1>
				<p>Please enter your unique customer ID or e-mail address. The system will send an 
				automatic e-mail response back to you with your access information. If you continue having problems accessing the on-line 
				support services, please contact the helpdesk by telephone.
				</p>
			<div class="spacer">&nbsp;</div>
		</div>	<!-- end of box content -->
	</div>
	<div class="boxFooter"><img src="../img/structure/box_footer_left.gif" /></div>
</div>

<div class="boxWrapper" style="margin:20px 20px 20px 20px; width:565px" ><img src="../img/structure/box_header_left.gif" width="6" height="11" alt="" border="0" /><div class="boxMiddle">
		<div class="boxContent">
			<div class="spacer">&nbsp;</div>

<table border="0" width="100%" cellpadding="5" cellspacing="0">
  <form method=post action="passwordrequest.php">
    <input type="hidden" name="request" value="passwordrequest">
    <tr>
      <td align="right" valign="middle"><strong>Customer ID or E-Mail Address :</strong></td>
      <td align="left"><input type="text" name="customerid" style="width:270px;"></td>
    </tr>
    <tr>
      <td align="right" valign="middle"><img src="/images/space.gif" width="2" height="2" alt="" border="0"></td>
      <td align="right"><br>
        <input type="submit" name="Submit" value="Request Password">
        &nbsp;</td>
  </FORM>
  </tr> 
</table>
			<div class="spacer">&nbsp;</div>
		</div>	<!-- end of box content -->
	</div>
	<div class="boxFooter"><img src="../img/structure/box_footer_left.gif" /></div>
</div>

<?php
}//end if customer id is null
else
{
	include("../_ssconfig.php");
	$con = swhd_wcopen(_SERVER_NAME,_INSTANCE_NAME);
	if($con < 33)
	{
		swhd_sendcommand($con, "CUSTOMER PASSWORD MAILBACK REQUEST " . $customerid);	
	}//end if con < 33
	swhd_close($con);
?>


<div class="boxWrapper" style="margin:20px 20px 20px 20px; width:565px" ><img src="../img/structure/box_header_left.gif" width="6" height="11" alt="" border="0" /><div class="boxMiddle">
		<div class="boxContent">
			<div class="spacer">&nbsp;</div>
				<h1>Password Reminder Request</h1>
				<p>Your Password has been sent to the registered address. Thank you for Using the Online Assistant.</p>
			<div class="spacer">&nbsp;</div>
		</div>	<!-- end of box content -->
	</div>
	<div class="boxFooter"><img src="../img/structure/box_footer_left.gif" /></div>
</div>

<?php
}//end else customer id is not null
?>

</body>
</html>


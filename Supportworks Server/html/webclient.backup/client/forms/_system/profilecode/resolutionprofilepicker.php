<?php
	//-- problemprofilepicker.php
	//-- 1.0.0
	//-- \apps\system\forms\pickers\profilecode

	//-- given filter will display a tree of problem profiles typically called by lfc

	//-- this is used with call forms and javascipt method to allow analyst to pick a code.
	//-- will callback function and pass in selected code and text


	//-- check session
	$excludeTokenCheck=true;
	include("../../../../php/session.php");

	function getChildProfileCodesXml($parentCode,$intLevel, $dbconn)
	{
		$strSelect = "select * from fixcode where parentcode='".db_pfs($parentCode)."' and levelx= ".$intLevel." order by descx asc";
		$strChildXML = "";
		$res = _execute_xmlmc_sqlquery($strSelect,$dbconn);
		while($row = hsl_xmlmc_rowo($res))
		{
			//-- create javascript
			$strChildXML.="<profilecode pid='".$row->parentcode."' levelx='".$row->levelx."' code='".$row->code."'><text>".pfx($row->descx)."</text></profilecode>";
			$strChildXML.=getChildProfileCodesXml($row->code,$row->levelx+1,$dbconn);
		}
		return $strChildXML;
	}


	//--
	//-- connect to swdata - get profile codes
	$swdata = connectdb("swdata");

	//-- 88568 only apply filter when selecting top level codes 
	//-- so need to get top level codes first and then get their children (without using filter)
	if(isset($_REQUEST['filter']) && $_REQUEST['filter']!="")
	{
		//-- get parent items
		$strSelect = "select * from fixcode where " . $_REQUEST['filter'] . " and parentcode='ROOT' order by descx asc";
		$resParents = _execute_xmlmc_sqlquery($strSelect,$swdata);
		$strXML = "<profilecodes>";
		while($row = hsl_xmlmc_rowo($resParents))
		{
			$strXML.="<profilecode pid='".$row->parentcode."' levelx='".$row->levelx."' code='".$row->code."'><text>".pfx($row->descx)."</text></profilecode>";
			$strXML.=getChildProfileCodesXml($row->code,1, $swdata);
		}
		$strXML .= "</profilecodes>";
	}
	else
	{
		//-- get all codes
		$strSelect = "select * from fixcode order by descx asc";
		$strXML = "<profilecodes>";
		$res = _execute_xmlmc_sqlquery($strSelect,$swdata);
		while($row = hsl_xmlmc_rowo($res))
		{
			//-- create javascript
			$strXML.="<profilecode pid='".$row->parentcode."' levelx='".$row->levelx."' code='".$row->code."'><text>".pfx($row->descx)."</text></profilecode>";
		}
		$strXML .= "</profilecodes>";
	}

?>
<html>
	<title>Choose Profile</title>
	<link rel="StyleSheet" href="tree.css" type="text/css" />

	<style>
		*
		{
			font-size:100%;font-family:Verdana,sans-serif;letter-spacing:.03em;
		}

		body
		{
			background-color:#dfdfdf;
		}

		#profile_tree
		{
			width:100%;
			height:100%;
			background-color:#ffffff;
			border-style:solid;
			border-width:1;
			border-color:#808080;
			overflow:auto;
			position:relative;
			top:4;
		}

		input
		{
			width:100%;
			height:18px;
			border-style:solid;
			border-width:1;
			border-color:#808080;
			background-color:#dfdfdf;
		}
		 textarea
		 {
			font-size:120%;
			width:100%;
			height:18px;
			border-style:solid;
			border-width:1;
			border-color:#808080;
			background-color:#dfdfdf;

		 }
		button
		{
			width:60px;
		}
		td
		{
			font-size:85%;
		}
		label
		{
			font-size:105%;
		}
		.cb
		{
			border-style:none;
			background-color:#dfdfdf;
		}

	</style>

	<script>
		var undefined;
		if(window.dialogArguments!=undefined)
		{
			var info = window.dialogArguments;
		}
		else
		{
			var info = opener.__open_windows[window.name];
		}	
		var app = info.__app;	
		var jqDoc = app.jqueryify(document); //-- so can use jquery
		
		//-- create tree object
		var profileTree;

		function process_profile_tree()
		{


			var oXmlProfileCodesInfo = app.create_xml_dom("<?php echo $strXML;?>");
			var aDiv = document.getElementById("profile_tree");

			profileTree = app.newtree('profileTree',document);
			profileTree.controlid = aDiv.id;
			profileTree.config.folderLinks = true; //-- folder behave link nodes when clicked
			profileTree.config.useCookies = false;
			profileTree.dc = apply_profile_code;
			profileTree.add("ROOT__0","-1","Resolution Profiles",select_profile_code,'','','treeimages/g-arrow-right.png','treeimages/g-arrow-right.png',true);

			 //-- load profile folders - get folders and then output tree control
			 var arrXFolders = oXmlProfileCodesInfo.getElementsByTagName("profilecode");
			 for(var x=0; x < arrXFolders.length;x++)
			{

				var strParentID = arrXFolders[x].getAttribute("pid");
				var strCodeID = arrXFolders[x].getAttribute("code");
				var intLevel = arrXFolders[x].getAttribute("levelx");
				var parentLevel = intLevel-1;
				if(parentLevel==-1)parentLevel = "0";

				var strText = app.xmlNodeTextByTag(arrXFolders[x],"text");
				profileTree.add(strCodeID +"__"+intLevel,strParentID+"__"+parentLevel,strText,select_profile_code,'','','treeimages/g-arrow-right.png','treeimages/g-arrow-right.png',false,true);
			}
			
			//-- output tree
			aDiv.innerHTML = profileTree;
			oXmlProfileCodesInfo = null;

			//-- select inital code
			var currCodePos = profileTree.getNodePositionByPathIds(info.__recordkey);
			if(currCodePos!=-1)	profileTree.openTo(currCodePos,true,true,true);
		}

		//-- load profile code information (sla, script, desc etc)
		function select_profile_code()
		{
			var eDesc = document.getElementById("profile.descx");
			var eCode = document.getElementById("profile.code");
			var eInfo = document.getElementById("profile.info");
			var eSLA = document.getElementById("profile.sla");
			var eScript = document.getElementById("profile.scriptname");
			var eFInfo = document.getElementById("flg.profile.info");
			var eFSLA = document.getElementById("flg.profile.sla");
			var eFScript = document.getElementById("profile.flags");

			if(eDesc!=null)eDesc.value = "";
			if(eCode!=null)eCode.value = "";
			if(eInfo!=null)eInfo.value = "";
			if(eSLA!=null)eSLA.value = "";
			if(eScript!=null)eScript.value = "";
			if(eFInfo!=null)
			{
				eFInfo.checked=false;
				eFInfo.disabled=true;
			}
			if(eFSLA!=null)
			{
				eFSLA.checked=false;
				eFSLA.disabled=true;
			}
			if(eFScript!=null)eFScript.checked = false;

			var strCode = profileTree.getNodePath("-");
			if(strCode!="")
			{
				//-- replace level indicators so get unique code
				strCode = strCode.replace("__0","");
				strCode = strCode.replace("__1","");
				strCode = strCode.replace("__2","");
				strCode = strCode.replace("__3","");
				strCode = strCode.replace("__4","");
				strCode = strCode.replace("__5","");
				strCode = strCode.replace("__6","");
				strCode = strCode.replace("__7","");
				strCode = strCode.replace("__8","");
				strCode = strCode.replace("__9","");
				strCode = strCode.replace("__10","");
				strCode = strCode.replace("__11","");

				//-- get profile code detail
				var strURL = app.get_service_url("profilecode/getresolutionprofile","");
				var strParams = "profilecode=" + strCode;
				app.get_http(strURL, strParams, true, true, profile_detail_loaded);
				
				//-- set description and code fields
				if(eDesc!=null)eDesc.value = profileTree.getNodeTextPath("->")
				if(eCode!=null)eCode.value = strCode;
			}
		}

		function profile_detail_loaded(oXML)
		{
		
			var rec = oXML.childNodes[0];
			for(var x=0;x<rec.childNodes.length;x++)
			{
				
				var aE = document.getElementById("profile."+rec.childNodes[x].tagName);
				if(aE!=null)
				{
					var strValue = app.xmlText(rec.childNodes[x]);
					//alert(strValue)
					app.setEleValue(aE,strValue);

					//-- check if has label and flag
					var bDis = (strValue!="")?false:true;
					var flagE = document.getElementById("flg." + aE.id);
					var labelE = document.getElementById("lbl." + aE.id);
					if(labelE!=null)
					{
						labelE.setAttribute("disabled",bDis);
					}
					if(flagE!=null)
					{
						//-- check default setting - 24.05.2011
						if(aE.id=="profile.info")
						{

							if(!app.session.IsDefaultOption(app.ANALYST_DEFAULT_AUTOFILLRESOLUTIONTEXT))
							{
								//-- not default option to select and use text so do not flag
								flagE.checked=false;			
							}
							else
							{
								//- -default is to use - but only check if we have data
								flagE.checked=(strValue!="")?true:false;			
							}
						}
						else
						{
							flagE.checked=(strValue!="")?true:false;			
						}
						flagE.setAttribute("disabled",bDis);
					}
				}
			}
			oXML = null;
		}

		//-- callback function to log call form
		var o = new Object();
		o.selected = false;
		o.code = "";
		o.codeDescription = "";
		o.description = "";
		o.sla = "";
		o.operatorScriptId = 0;
		function apply_profile_code()
		{
			//-- pass back info
			var bUseInfo = document.getElementById("flg.profile.info").checked;
			var strInfo = (bUseInfo)?document.getElementById("profile.info").value:"";
			//var strInfo = document.getElementById("profile.info").value;



			o.selected = true;

			var eCode = document.getElementById("profile.code");
			o.code = eCode.value;

			o.codeDescription = profileTree.getNodeTextPath("->");
			o.description = strInfo;
			o.sla = "";
			o.operatorScriptId = 0;
			app.__open_windows[window.name].returnInfo = o;


			self.close();
		}

		function form_close()
		{
			app.__open_windows[window.name].returnInfo = o;
		}

	</script>

	<body onload="process_profile_tree();" onbeforeunload="form_close();"  onunload="app._on_window_closed(window.name)">	
		<table width="100%" height="100%">
		<tr><td>
			<table width="100%">
				<tr>
					<td>Profile :</td>
					<td colspan="3"><input type="text" id='profile.descx'></td>
				</tr>
				<tr>
					<td align="right" >Code :</td>
					<td width="100%"><input type="text" id='profile.code'></td>
				</tr>
			</table>
		</td></tr>
		<tr><td height="100%">
			<table height="100%" width="100%">
				<tr>
					<td>Browse Profiles</td>
					<td>Default Description</td>
				</tr>
				<tr>
					<td height="100%" width="50%" valign="top">
						<div id='profile_tree'></div>
					</td>
					<td height="100%" width="50%" valign="top">
						<table  width="100%" height="100%">
							<tr>
								<td  width="100%" height="99%" colspan="3" valign="top">
									<textarea id='profile.info' style='height:99%' readonly></textarea>
								</td>
							</tr>
							<tr>
								<td width="20px" noWrap>
									<input type='checkbox' class="cb" id='flg.profile.info'>
								</td>
								<td width="100%" colspan="2">
									<label id='lbl.profile.info' for="flg.profile.info" disabled disabled>Transfer this default description to the form</label>
								</td>
							</tr>
							<tr>
								<td colspan="3" align="right">
									<button onclick="apply_profile_code();">OK</button>&nbsp;&nbsp;<button onclick="self.close();">Cancel</button>
								</td>
							</tr>

						</table>
					</td>
				</tr>
			</table>
		</td></tr>
		</table>
	</body>
</html>
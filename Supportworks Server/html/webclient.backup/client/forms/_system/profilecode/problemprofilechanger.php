<?php
	//-- problemprofilechanger.php
	//-- 1.0.0
	//-- \apps\system\forms\pickers\profilecode

	//-- given currentcode and filter will display a tree of problem profiles typically called by call detail form in order to change profile code

	//-- check session
	$excludeTokenCheck=true;
	include("../../../../php/session.php");


	function getChildProfileCodesXml($parentCode,$intLevel, $dbconn,$parentPath)
	{
		$strSelect = "select * from probcode where parentcode='".db_pfs($parentCode)."' and levelx= ".$intLevel." order by descx asc";
		$strChildXML = "";
		$res = hsl_odbc_exec($dbconn,$strSelect);
		while($row = hsl_odbc_fetch_row($res))
		{
			//-- create javascript
			$fullPath = $parentPath ."-".$row->code;
			$strChildXML.="<profilecode fullpath='".$fullPath."' pid='".$row->parentcode."' levelx='".$row->levelx."' code='".$row->code."'><text>".pfx($row->descx)."</text></profilecode>";
			$strChildXML.=getChildProfileCodesXml($row->code,$row->levelx+1,$dbconn,$fullPath);
		}
		return $strChildXML;
	}


	//--
	//-- connect to swdata - get profile codes
	$swdata = connectdb("swdata");

	//-- 88568 only apply filter when selecting top level codes 
	//-- so need to get top level codes first and then get their children (without using filter)
	if(isset($_REQUEST['filter']) && $_REQUEST['filter']!="")
	{
		//-- get parent items
		$strSelect = "select * from probcode where " . $_REQUEST['filter'] . " and parentcode='ROOT' order by descx asc";
		$resParents = hsl_odbc_exec($swdata,$strSelect);
		$strXML = "<profilecodes>";
		while($row = hsl_odbc_fetch_row($resParents))
		{
			$strXML.="<profilecode fullpath='".$row->code."' pid='".$row->parentcode."' levelx='".$row->levelx."' code='".$row->code."'><text>".pfx($row->descx)."</text></profilecode>";
			$strXML.=getChildProfileCodesXml($row->code,1, $swdata,$row->code);
		}
		$strXML .= "</profilecodes>";
	}
	else
	{

		//-- get parent items
		$strSelect = "select * from probcode where parentcode='ROOT' order by descx asc";
		$resParents = hsl_odbc_exec($swdata,$strSelect);
		$strXML = "<profilecodes>";
		while($row = hsl_odbc_fetch_row($resParents))
		{
			$strXML.="<profilecode fullpath='".$row->code."' pid='".$row->parentcode."' levelx='".$row->levelx."' code='".$row->code."'><text>".pfx($row->descx)."</text></profilecode>";
			$strXML.=getChildProfileCodesXml($row->code,1, $swdata,$row->code);
		}
		$strXML .= "</profilecodes>";
	}


?>
<html>
	<title>Change Profile</title>
	<link rel="StyleSheet" href="tree.css" type="text/css" />

	<style>
		*
		{
			font-size:100%;font-family:Verdana,sans-serif;letter-spacing:.03em;
		}

		body
		{
			background-color:#dfdfdf;
		}

		#profile_tree
		{
			width:100%;
			height:100%;
			background-color:#ffffff;
			border-style:solid;
			border-width:1;
			border-color:#808080;
			overflow:auto;
			position:relative;
			top:4;
		}

		input
		{
			width:100%;
			height:18px;
			border-style:solid;
			border-width:1;
			border-color:#808080;
			background-color:#dfdfdf;
		}
		button
		{
			width:60px;
		}
		td
		{
			font-size:85%;
		}
		label
		{
			font-size:90%;
		}



	</style>

	<script>
		var undefined;
		if(window.dialogArguments!=undefined)
		{
			var info = window.dialogArguments;
		}
		else
		{
			var info = opener.__open_windows[window.name];
		}	
		var app = info.__app;
		var jqDoc = app.jqueryify(document); //-- so can use jquery
		//-- create tree object
		var profileTree;

		function process_profile_tree()
		{
			var oXmlProfileCodesInfo = app.create_xml_dom("<?php echo $strXML;?>");
			var aDiv = document.getElementById("profile_tree");

			profileTree = app.newtree('profileTree',document);
			profileTree.controlid = aDiv.id;
			profileTree.config.folderLinks = true; //-- folder behave link nodes when clicked
			profileTree.config.useCookies = false;
			profileTree.dc = apply_profile_code;
			profileTree.add("ROOT__0","-1","Problem Profiles",select_profile_code,'','','treeimages/g-arrow-right.png','treeimages/g-arrow-right.png',true);

			 //-- load profile folders - get folders and then output tree control
			 var arrXFolders = oXmlProfileCodesInfo.getElementsByTagName("profilecode");
			 for(var x=0; x < arrXFolders.length;x++)
			{
				var strParentID = arrXFolders[x].getAttribute("pid");
				var strCodeID = arrXFolders[x].getAttribute("code");
				var intLevel = arrXFolders[x].getAttribute("levelx");
				var parentLevel = intLevel-1;
				if(parentLevel==-1)parentLevel = "0";

				var nodeID = arrXFolders[x].getAttribute("fullpath");
				var arrInfo = nodeID.split("-");
				arrInfo.pop();
				parentNodeID = arrInfo.join("-");
				if(parentNodeID=="")parentNodeID = "ROOT__0";

				var strText = app.xmlNodeTextByTag(arrXFolders[x],"text");
				profileTree.add(nodeID,parentNodeID,strText,select_profile_code,'','','treeimages/g-arrow-right.png','treeimages/g-arrow-right.png',false,true);

			}
			
			//-- output tree
			aDiv.innerHTML = profileTree;
			oXmlProfileCodesInfo = null;

			//-- highlight current code
			var currCodePos = profileTree.getNodePositionByPathIds(info.__recordkey);
			if(currCodePos!=-1)	profileTree.openTo(currCodePos,true,true,false);
		}

		//-- load profile code information (sla, script, desc etc)
		function select_profile_code()
		{
			var eCode = document.getElementById("profile.code");

			var strCode = profileTree.getSelectedNode().id;

			if(strCode!="")
			{
				//-- set description and code fields
				if(eCode!=null)
				{
					strCode = strCode.replace("__0","");
					strCode = strCode.replace("__1","");
					strCode = strCode.replace("__2","");
					strCode = strCode.replace("__3","");
					strCode = strCode.replace("__4","");
					strCode = strCode.replace("__5","");
					strCode = strCode.replace("__6","");
					strCode = strCode.replace("__7","");
					strCode = strCode.replace("__8","");
					strCode = strCode.replace("__9","");
					strCode = strCode.replace("__10","");
					strCode = strCode.replace("__11","");
					
					eCode.value = strCode;
				}
			}
			else
			{
				if(eCode!=null)eCode.value = "";
			}
		}


		//-- callback function to log call form
		var o = new Object();
		o.selected = false;
		o.code = "";
		o.codeDescription = "";
		o.description = "";
		o.sla = "";
		o.operatorScriptId = 0;
		function apply_profile_code()
		{
			//-- pass back info to calling function
			var eCode = document.getElementById("profile.code");
			o.selected = true;
			o.code = eCode.value;
			o.codeDescription = profileTree.getNodeTextPath("->");
			o.description = "";
			o.sla = "";
			o._additional_slaid = "";
			o._additional_slaname = "";
			o.operatorScriptId = 0;
			app.__open_windows[window.name].returnInfo = o;
	
			self.close();
		}

		function form_close()
		{
			app.__open_windows[window.name].returnInfo = o;
		}


	</script>

	<body onload="process_profile_tree();" onbeforeunload="form_close();"  onunload="app._on_window_closed(window.name)">
		<table width="100%" height="100%">
		<tr><td>
			<table width="100%">
				<tr>
					<td width="50%"><label>Current Code :</label><br>
					<input type="text" value="<?php echo $_REQUEST['initcode'];?>"></td>
					<td  width="50%"><label>Change To :</label><br>
					<input type="text" id='profile.code'></td>
				</tr>
			</table>
		</td></tr>
		<tr><td height="100%">
			<table height="100%" width="100%">
				<tr>
					<td><label>Select a profile from the tree below</label></td>
				</tr>
				<tr>
					<td height="100%" width="100%" valign="top">
						<div id='profile_tree'><br><br><center>... loading ...</center></div>
					</td>
				</tr>
				<td align="right"><br><button onclick="apply_profile_code();">OK</button>&nbsp;&nbsp;<button onclick="self.close();">Cancel</button></td>
				</tr>
			</table>
		</td></tr>
		</table>
	</body>
</html>
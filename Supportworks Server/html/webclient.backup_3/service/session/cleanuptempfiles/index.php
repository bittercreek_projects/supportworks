<?php

	//-- 22.03.2010 - clean up temp files
	//-- expects :-
	//--			analystid:
	//--			swsessionid :

	//-- use sw xmlrpc to logout analyst ??
	@session_destroy();

	include("SwPhpDll.php");

	set_time_limit(15);
	
	//-- FUNCTIONS
	function deletepath($strPath)
	{
		if($handle = @opendir($strPath)) 
		{ 
			while($file = @readdir($handle)) 
			{ 
				$filePath =$strPath.'/'.$file; 
				if(is_file($filePath)) 
				{
					@unlink($filePath);
				}
				else 
				{
					if($file!="." && $file!="..")
					{
						if(@opendir($filePath))deletepath($filePath);
					}
				}
				clearstatcache(); 
			} 
			closedir($handle); 
		}
		@rmdir($strPath);
	}
	//-- EOF FUNCTIONS

	chdir("../../../");
	$currPath = getcwd();

	//-- get temp location
	$tempFolderPath = $currPath ."/temporaryfiles/";
	$destination_path =  $tempFolderPath . $_POST["swsessionid"];
	$destination_path = @str_replace("\\","/",$destination_path);
	deletepath($destination_path);

	//-- now check any expired session where folder still exists
	$strSessionFolderList="";
	$destination_path = @str_replace("\\","/",$tempFolderPath);
	if($handle = @opendir($destination_path)) 
	{ 
		$arrFolderSessions = Array();
		while($file = @readdir($handle)) 
		{ 
			$filePath = $destination_path.'/'.$file; 
			if(!is_file($filePath)) 
			{
				if($file!="." && $file!="..")
				{
					//-- is a dir - so will be named after a session - store in var
					if($strSessionFolderList!="")$strSessionFolderList.=",";
					$strSessionFolderList.="'".$file."'";
					$arrFolderSessions[]=$file;
				}
			}
		} 
		closedir($handle); 

		
		if($strSessionFolderList!="")
		{
			//-- fetch sessions from sessions table
			$strSQL = "select sessionid from swsessions where sessionid in(".$strSessionFolderList.")";
			$res = @_execute_xmlmc_sqlquery($strSQL, "sw_systemdb");
			if($row = hsl_xmlmc_rowo($result_id))
			{
				
				if($res)
				{
					//-- get list of sessionids and remove any matches from session string
					$rs = array();
					$rows = Array();
					$rs_obj = false;

					while($rs = hsl_xmlmc_rowo($result_id))
					{
						foreach($rs as $key=>$value )
						{
							$fkey = strtolower($key);
							$rs_obj->$fkey = trim($value);
						}
						$rows[]=$rs_obj;
					}

					//-- if count is 0 then remove all items in 
					if(count($rows)==0)
					{
						for($x=0;$x<count($arrFolderSessions);$x++)
						{
							deletepath($tempFolderPath .$arrFolderSessions[$x]);
						}
					}
					else
					{
						//-- loop returned sessionid. Those that are not in our array should have path deleted
						for($x=0;$x<count($rows);$x++)
						{
							//-- session in table does not have a folder so exit
							if(!in_array($rows[$x]->sessionid,$arrFolderSessions))
							{
								//-- delete path
								deletepath($tempFolderPath .$rows[$x]->sessionid);
							}
						}
					}
				}
			}
		}
	}
	exit();
?>
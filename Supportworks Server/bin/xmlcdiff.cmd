@echo off
set filename=%1
shift
set newfile=%filename%.decoded

xmlc.exe -d -i %filename% -o %newfile% && cls & type %newfile% & del %newfile%
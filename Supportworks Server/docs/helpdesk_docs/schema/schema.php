<?php	//-- TK End Server Version Check
	$sessid = gv('sessid');
	//-- F0100079 - check session variable
	if((!regex_match("/^[a-zA-Z0-9]{14}-[a-zA-Z0-9]{4,5}-[a-zA-Z0-9]{8}$/",$sessid)) && (!regex_match("/^[a-zA-Z0-9]{8}-[a-zA-Z0-9]{8}-[a-zA-Z0-9]{4}$/",$sessid)))
	{
		//-- call not found ?? in theory should never happen
		?>
		<html>
			<head>
				<meta http-equiv="Pragma" content="no-cache">
				<meta http-equiv="Expires" content="-1">
				<title>Support-Works Call Search Failure</title>
					<link rel="stylesheet" href="sheets/maincss.css" type="text/css">
			</head>
				<body>
					<br></br>
					<center>
					<span class="error">
						A submitted variable was identified as a possible security threat.<br> 
						Please contact your system Administrator.
					</span>
					</center>
				</body>
		</html>
		<?php
		exit;
	}	
	
	//$bInWebclient = $GLOBALS['_webclient'];
	$bInWebclient = gv('_webclient');

	//-- Construct a new active page session
	$session = new classActivePageSession($sessid);

	//-- Initialise the session
	if(!$session->IsValidSession())
	{
	?>
		<html>
			<head>
				<meta http-equiv="Pragma" content="no-cache">
				<meta http-equiv="Expires" content="-1">
				<title>Support-Works Session Authentication Failure</title>
					<link rel="stylesheet" href="sheets/maincss.css" type="text/css">
			</head>
				<body>
					<br><br>
					<center>
						<span class="error">
							There has been a session authentication error<br>
							Please contact your system administrator.
						</span>
					</center>
				</body>
		</html>
	<?php
		exit;
	}
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"  "http://www.w3.org/TR/html4/loose.dtd">
<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <title>Model Report</title>
</head>
<frameset rows="70,*">
  <frame src = "top.html" name="title" noresize scrolling="no" frameborder="0" marginheight="0" marginwidth="0">
  <frameset cols="25%,*" border="0">
    <frame src = "overview_list.html" name="overview" scrolling="auto" frameborder="0" marginheight="0" marginwidth="0">
    <frame src = "overview.html" name="content" scrolling="auto" frameborder="0" marginheight="0" marginwidth="0">
    
    <noframes>
    <body>
      <p>This browser does not support frames. Please try Mozilla Firefox.</p>
    </body>
    </noframes>
  </frameset>
</frameset>
</html>

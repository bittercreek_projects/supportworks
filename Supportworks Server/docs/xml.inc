<?php

if (version_compare(PHP_VERSION,'5','>='))
	require_once('domxml-php4-to-php5.php'); //Load the PHP5 converter

//--
//-- XMLDOM HELPERS

//--
//-- get node attribute function as xml_node class does not provide one
function getAttribute($name, $att)
{
	foreach($att as $attkey => $anAttribute)
	{
		if($anAttribute->name()==$name)return $anAttribute->value();        
	}
}

function GetNodeText($node) 
{    
	$st = "";
	foreach ($node->child_nodes() as $cnode)
	{
		if ($cnode->node_type()==XML_TEXT_NODE)
		{
			$st .= $cnode->node_value();
		}
     }
     return trim($st); //-- remove whitespace
}


//--
//-- XPATH HELPERS

//--
//-- return xmlNode based on id and xpath
function xml_element_by_id($dom, $str_id  )
{
	$node_with_id  = null;
	$xpath = xpath_new_context($dom);
	$xpresult = xpath_eval($xpath, "//@id");
	foreach( $xpresult->nodeset as $node )
	{
		if ($node->value == $str_id) return $node->parent_node();
   }
   return $node_with_id;
}


?>